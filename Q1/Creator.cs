﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns
{
    /// <summary>
    /// Общая фабрика
    /// </summary>
    public class Creator
    {
        public class BindingModel
        {
            public DataModels.DB.IDB _DB = null;
            public Logic.IAddressFinder _addressFinder = null;
            public Logic.IHttpContextHandler _httpContextHandler = null;
            public TuravIns.API.IPostRequest _postRequest = null;
            public TuravIns.API.ISberbank _sberbank = null;
        }

        public static BindingModel binding = new BindingModel();

        public static DataModels.DB.IDB DB { get { return binding._DB ?? new DataModels.DB.DB(); } }
        public static Logic.IAddressFinder addressFinder { get { return binding._addressFinder ?? new Logic.AddressFinder(); } }
        public static Logic.IHttpContextHandler httpContextHandler { get { return binding._httpContextHandler ?? new Logic.HttpContextHandler(); } }
        public static TuravIns.API.IPostRequest postRequest { get { return binding._postRequest ?? new TuravIns.API.PostRequest(); } }
        public static TuravIns.API.ISberbank sberbank { get { return binding._sberbank ?? new TuravIns.API.Sberbank(); } }
    }
}