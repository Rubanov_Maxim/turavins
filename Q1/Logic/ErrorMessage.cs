﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns.Logic
{
    /// <summary>
    /// свалка сообщений для ошибок общего назначения
    /// https://dwprdev.atlassian.net/wiki/spaces/APIENGINE/pages/439943477/API
    /// </summary>
    public class ErrorMessage
    {
        /// <summary>
        /// Запрос из внешнего Api
        /// </summary>
        public static string ERROR_EXTERNAL_API_REQUEST = "Запрос из внешнего Api";
        /// <summary>
        /// Для выбранной СК дата начала не может быть равна текущей дате
        /// </summary>
        public static string GUARD_CLAUSE_TODAY = "Для выбранной СК дата начала не может быть равна текущей дате";
        /// <summary>
        /// Дата окончания действия полиса не может быть раньше даты начала
        /// </summary>
        public static string GUARD_CLAUSE_INVALID_END_DATE = "Дата окончания действия полиса не может быть раньше даты начала";
        /// <summary>
        /// Передано недопустимое значение параметра {0}: {1}
        /// </summary>
        public static string GUARD_CLAUSE_INVALID_PARAMETER = "Передано недопустимое значение параметра {0}: {1}";
        /// <summary>
        /// Передан риск (id = {0}), для которого отсутствуют настройки для данного значения isMultiple в данной СК
        /// </summary>
        public static string GUARD_CLAUSE_INVALID_RISK_FOR_ISMULTIPLE = "Передан риск (id = {0}), для которого отсутствуют настройки для данного значения isMultiple в данной СК";
        /// <summary>
        /// Передано недопустимое значение параметров для поиска в таблице {0}
        /// </summary>
        public static string GUARD_CLAUSE_INVALID_PARAMETER_INTABLE = "Передано недопустимое значение параметров для поиска в таблице {0}";
        /// <summary>
        /// Переданный массив {0} не содержит элементов
        /// </summary>
        public static string GUARD_CLAUSE_EMPTY_ARRAY = "Переданный массив {0} не содержит элементов";
        /// <summary>
        /// Не передан обязательный риск 'Медицинские расходы'
        /// </summary>
        public static string GUARD_CLAUSE_NO_MEDICAL_RISK = $"Не передан обязательный риск 'Медицинские расходы' (id = {(int)enumTravelRisk.medicalExpenses})";
        /// <summary>
        /// Котировки {0} не существует
        /// </summary>
        public static string GUARD_CLAUSE_QUOTE_DOESNT_EXIST = "Котировки {0} не существует";
        /// <summary>
        /// Дата начала действия полиса не может быть раньше {0}
        /// </summary>
        public static string GUARD_CLAUSE_INVALID_START_DATE = "Дата начала действия полиса не может быть раньше {0}";
        /// <summary>
        /// Не передана сумма покрытия для риска id = {0}
        /// </summary>
        public static string GUARD_CLAUSE_RISK_WITHOUT_SUM = "Не передана сумма покрытия для риска id = {0}";
        /// <summary>
        /// Страхователь должен быть старше 17 лет
        /// </summary>
        public static string GUARD_CLAUSE_UNDERAGE_INSURER = "Страхователь должен быть старше 17 лет";
        /// <summary>
        /// Сумма страхового покрытия должна быть от 2 000 000 рублей по курсу ЦБ на дату расчета
        /// </summary>
        public static string GUARD_CLAUSE_MEDICAL_INSURANCE_SUM = "Сумма страхового покрытия должна быть от 2 000 000 рублей по курсу ЦБ на дату расчета";
        /// <summary>
        /// Котировки {0} не существует
        /// </summary>
        public static string ERROR_QUOTE_DOESNT_EXIST = "Котировки {0} не существует";
        /// <summary>
        /// ФИО туриста/покупателя должны быть переданы латиницей
        /// </summary>
        public static string GUARD_CLAUSE_NAMES_IN_CYRILLIC = "ФИО туриста/покупателя должны быть переданы латиницей";
        /// <summary>
        /// Расчеты с признаком \"многократный полис\" выполняются только для стран Шенгенского соглашения
        /// </summary>
        public static string GUARD_CLAUSE_SCHENGEN_COUNTRY_ONLY = "Расчеты с признаком \"многократный полис\" выполняются только для стран Шенгенского соглашения";
        /// <summary>
        /// Расчеты с признаком \"многократный полис\" допустимы только с суммами страхования 35000 и 50000
        /// </summary>
        public static string GUARD_CLAUSE_ABSOLUTE_MULTIPLE_SUMS = "Расчеты с признаком \"многократный полис\" допустимы только с суммами страхования 35000 и 50000";
        /// <summary>
        ///В многократные полисы уже включено страхование Задержки рейса на 500 и страхование юридической помощи на 500
        /// </summary>
        public static string GUARD_CLAUSE_ABSOLUTE_INVALID_RISK_SUM = "В многократные полисы уже включено страхование Задержки рейса на 500 и страхование юридической помощи на 500";

        /// <summary>
        /// Нулевая стоимость полиса в страховой компании
        /// </summary>
        public static string ERROR_POLICY_PRICE_IS_ZERO = "Нулевая стоимость полиса в страховой компании";
        /// <summary>
        /// Перед акцептацией нужно сохранить полис
        /// </summary>
        public static string GUARD_CLAUSE_POLICY_IS_NOT_SAVED = "Перед акцептацией нужно сохранить полис";
        /// <summary>
        /// Для получения бланка полиса нужно сохранить и акцептовать полис
        /// </summary>
        public static string GUARD_CLAUSE_POLICY_IS_NOT_ACCEPTED = "Для получения бланка полиса нужно сохранить и акцептовать полис";
        /// <summary>
        /// Текущий статус полиса не предполагает его аннуляции
        /// </summary>
        public static string GUARD_CLAUSE_POLICY_CANT_BE_ANNULED = "Текущий статус полиса не предполагает его аннуляции";

        /// <summary>
        /// В URL'е запроса не передан rKey или передан пустой rKey
        /// </summary>
        public static string APIERRORDESCRIPTION_UNAUTORESATION_NORKEY = "Не передан обязательный параметр rKey";
        /// <summary>
        /// В URL'е запроса (кроме авторизации) не передан token
        /// </summary>
        public static string APIERRORDESCRIPTION_UNAUTORESATION_NOTOKEN = "Не передан обязательный параметр token";
        /// <summary>
        /// В URL'е запроса (кроме авторизации) передан несуществующий или устаревший token
        /// Устаревшим считается токен в двух случаях:
        /// С момента выпуска прошло больше времени время жизни токена
        /// С момента выпуска этим же агентом был выпущен новый токен
        /// </summary>
        public static string APIERRORDESCRIPTION_UNAUTORESATION_WRONGTOKEN = "Использован несуществующий или устаревший token";
        /// <summary>
        /// Переданный токен валиден, но пренадлежит заблокированному агенту
        /// </summary>
        public static string APIERRORDESCRIPTION_UNAUTORESATION_INACTIVEAGENTSTOKEN = "Доступ закрыт";
        /// <summary>
        /// Переданный логин и пароль не нашелся среди записей активных (не заблокированных) агентов в таблице agent
        /// </summary>
        public static string APIERRORDESCRIPTION_UNAUTORESATION_NOAGENTINDB = "Некорректные параметры авторизации";
        /// <summary>
        /// Вызван несуществующий метод
        /// </summary>
        public static string APIERRORDESCRIPTION_METHODNOTFOUND = "Вызванный метод не существует";
        /// <summary>
        /// Ошибка при получении курса валют в TuravIns.Logic.Helper
        /// </summary>
        public static string HELPERMODELEXCEPTION_CURRENCYEXCHEANGEERROR = "Ошибка при получении курса валют";

        /// <summary>
        /// Не найдены настройки страхования для данного агента в данной страховой компании
        /// </summary>
        public static string ERROR_NO_INSURANCE_SETTINGS = "Не найдены настройки страхования для данного агента в данной страховой компании";
        /// <summary>
        /// Рениссанс: не удалось получить policyId
        /// </summary>
        public static string ERROR_RENISSANS_NO_POLICY_ID = "Рениссанс: не удалось получить policyId";
        /// <summary>
        /// Страховая компания insuranceCompanyId:{0} не доступна для расчета
        /// </summary>
        public static string API_ERROR_COMPANY_IS_NOT_ACTIVE = "Страховая компания insuranceCompanyId:{0} не доступна для расчета";
        /// <summary>

        /// TRAVEL api переданное значение не найдено в бд
        /// </summary>
        public static string ERROR_TRAVELAPI_NOTVALUEINDB = "Не найдено значение {0}:{1} в бд";

        #region ошибки специфические для guideh 
        /// <summary>
        /// 400 - Недопустимый параметр, либо значение параметра в запросе
        /// </summary>
        public static string GUIDEH_ERROR_INVALID_PARAMETER = "Недопустимый параметр, либо значение параметра в запросе";
        /// <summary>
        /// 401 - Вы не авторизованы, или действие сессии закончилось
        /// </summary>
        public static string GUIDEH_ERROR_INVALID_TOKEN = "Вы не авторизованы, или действие сессии закончилось";
        /// <summary>
        /// 500 - Ошибка на стороне сервера СК Гайде
        /// </summary>
        public static string GUIDEH_ERROR_SERVER_FAILURE = "Ошибка на стороне сервера СК Гайде";
        #endregion

        /// Не передан обязательный параметр section getFile FileController
        /// </summary>
        public static string ERROR_FILEGET_NOT_SECTION = "Не передан обязательный параметр section";
        /// <summary>
        /// Не передан обязательный параметр fileId getFile FileController
        /// </summary>
        public static string ERROR_FILEGET_NOT_FILEID = "Не передан обязательный параметр fileId";
        /// <summary>
        /// В аргументах запроса передано недопустимое значение для section getFile FileController
        /// </summary>
        public static string ERROR_FILEGET_NOT_VALID_SECTION = "Недопустимое значение параметра section";
        /// <summary>
        /// Значение fileId не найдено в travelPolicy.guid getFile FileController
        /// </summary>
        public static string ERROR_FILEGET_NOT_FILE_FOUND = "Файл с таким идентификатором несуществует";
        /// <summary>
        /// начение fileId найдено в travelPolicy.guid но статус полиса Аннулирован (statusId=4) getFile FileController
        /// </summary>
        public static string ERROR_FILEGET_POLICE_ANNULATED = "Полис аннулирован";
        /// <summary>
        /// Неизвестная ошибка от СК
        /// </summary>
        public static string UNKNOWN_ERROR_SK = "В ответе страховой компании вернулась неизвестная ошибка"; 
        /// <summary>
        /// Для согласия туристов должно быть не больше 5
        /// </summary>
        public static string ERROR_SOGLASIE_INSURED = "Для выбранной СК количество туристов должно быть не больше 5";

        /// <summary>
        /// Для выбранной СК не поддерживается метод аннулирования
        /// </summary>
        public static string ERROR_ANNUL_NOT_AVAILABLE_FOR_INSURANCE_COMPANY = "Для выбранной СК не поддерживается метод аннулирования";
        /// <summary>
        /// Гайде: Неправильный запрос
        /// </summary>
        public static string GUIDEH_ERROR_INCORRECTRQUEST = "Гайде: Неправильный запрос";
    }
}