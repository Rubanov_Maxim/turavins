﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModels.DB;

namespace InsApi.Logic
{
    public class Creator
    {
        static List<BindingModel> bindings;
        public Creator()
        {
            bindings = new List<BindingModel>();
            bindings.Add(new BindingModel() { interfaceType = typeof(IDB), classType = typeof(DB) });
            bindings.Add(new BindingModel() { interfaceType = typeof(IHttpContextHandler), classType = typeof(HttpContextHandler) });
        }

        public Creator(List<BindingModel> _bindings)
        {
            bindings = _bindings;
        }


        public static T interfaceGet<T>()
        {
            Type interfaceType = typeof(T);
            BindingModel bindingModelFound = bindings.First(x => x.interfaceType == interfaceType);
            object instanceofType = null;
            if (bindingModelFound.mockClass != null)
            {
                instanceofType = bindingModelFound.mockClass;
            }
            else
            {
                instanceofType = Activator.CreateInstance(bindingModelFound.classType);
            }
            return (T)instanceofType;
        }

        public static List<T> listInterfaceGet<T>()
        {
            Type interfaceType = typeof(T);
            List<BindingModel> listClassType = bindings.FindAll(x => x.interfaceType == interfaceType);
            List<T> listInstanceofType = new List<T>();
            foreach (BindingModel bindingModel in listClassType)
            {
                object instanceofType = null;
                if (bindingModel.mockClass != null)
                {
                    instanceofType = bindingModel.mockClass;
                }
                else
                {
                    instanceofType = Activator.CreateInstance(bindingModel.classType);
                }
                listInstanceofType.Add((T)instanceofType);
            }
            return listInstanceofType;
        }
        public static IDB DB { get { return interfaceGet<IDB>(); } }
        public static IHttpContextHandler HttpContextHandler { get { return interfaceGet<IHttpContextHandler>(); } }

        public class BindingModel
        {
            public Type interfaceType { get; set; }
            public Type classType { get; set; }
            public object mockClass { get; set; }
        }
    }
}