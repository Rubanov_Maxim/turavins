﻿using System.Collections.Generic;
using TuravIns.Models;

namespace TuravIns.Logic
{
    public static class Dictionaries
    {
        /// <summary>
        /// Список стран, которые выделены
        /// </summary>
        public static List<int> listHighlightedCountry = new List<int>()
        {
            36,     //Весь мир
            159,    //США
            163,    //Таиланд
            188     //Шенген
        };

        /// <summary>
        /// https://dwprdev.atlassian.net/wiki/spaces/APIENGINE/pages/439943477/API
        /// Описание всех ошибок внешнего Api
        /// </summary>
        public static Dictionary<string, enumErrorType> dictionaryErrorDescr = new Dictionary<string, enumErrorType>()
        {
            //В URL'е запроса не передан rKey или передан пустой rKey
            { "Не передан обязательный параметр rKey", enumErrorType.badRequest },
            //В URL'е запроса (кроме авторизации) не передан token
            { "Не передан обязательный параметр token", enumErrorType.notFound},
            //В URL'е запроса (кроме авторизации) передан несуществующий или устаревший token
            { "Использован несуществующий или устаревший token", enumErrorType.unauthorized},
            //Переданный токен валиден, но пренадлежит заблокированному агенту
            { "Доступ закрыт", enumErrorType.unauthorized},
            //Отсутствие в модели запроса значения параметра (или самого параметра) который указан как обязательный в спецификации запроса
            { "Не передан обязательный параметр ", enumErrorType.badRequest},
            //В качестве идентификатора одного из параметров метода передан id которого нет в справочнике для этого параметра
            { "Значение параметра {0}: {1} отсутствует в справочнике", enumErrorType.notFound},
            //При обработке запроса случилась ошибка внутри системы sql exception и пр.
            { "Временные перебои в работе сервиса",enumErrorType.serverError },
            //Вызван несуществующий метод
            { "Вызванный метод не существует", enumErrorType.methodNotFound},
            //если нам понадобится отключать всех пользователей на время каких-нибудь обновлений, будем выдавать такой ответ
            {"Система на техническом обслуживании", enumErrorType.temporarilyUnavailable },
            //В модели запроса передано значение в неверном типе или формате(не в том который указан в спецификации метода)
            { "Значение передано в некорректном формате.Правильный формат ", enumErrorType.badRequest}
        };

        /// <summary>
        /// Названия папок для сохранения полисов от типа страхования
        /// </summary>
        public static Dictionary<enumInsuranceType, string> dictionaryInsuranceTypeFolder = new Dictionary<enumInsuranceType, string>()
        {
            { enumInsuranceType.Travel, "travelInsurance" },

        };

        /// <summary>
        /// Названия папок для сохранения полисов от СК
        /// </summary>
        public static Dictionary<enumInsuranceCompany, string> dictionaryInsuranceCompanyFolder = new Dictionary<enumInsuranceCompany, string>()
        {
            { enumInsuranceCompany.Alpha, "alfastrah" },
            { enumInsuranceCompany.Renissans, "renessans" },
            { enumInsuranceCompany.Guideh, "guideh" },
            { enumInsuranceCompany.Soglasie, "soglasie" },
            { enumInsuranceCompany.Absolute, "absolut" },
            { enumInsuranceCompany.Tinkoff, "tinkoff" }
        };
    }
}