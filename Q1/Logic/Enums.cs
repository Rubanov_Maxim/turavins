﻿namespace TuravIns.Logic
{
    public enum enumCountry
    {
        finland = 178,
        shengen = 188, 
        allWorld = 36
    }

    
    /// <summary>
    /// Тип страхования
    /// </summary>
    public enum enumInsuranceType
    {
        /// <summary>
        /// ВЗР
        /// </summary>
        Travel,
        /// <summary>
        /// Недвижимость
        /// </summary>
        Realty,
        /// <summary>
        /// Несчастный случай
        /// </summary>
        Accident,
        /// <summary>
        /// Осаго
        /// </summary>
        Osago,
        /// <summary>
        /// Каско
        /// </summary>
        Kasko,
        /// <summary>
        /// Зелёная карта
        /// </summary>
        GreenCard,
        /// <summary>
        /// Задержка рейса
        /// </summary>
        FlightDelay
    }

    /// <summary>
    /// секция для поиска файла на фтп (должен совпадать по названию с параметром section)
    /// </summary>
    public enum enumSectionFile
    {
        insuranceTravel 
    }

    /// <summary>
    /// id страховой компании
    /// </summary>
    public enum enumInsuranceCompany
    {
        /// <summary>
        /// Была странная потребность в необходимости 0 в перечислениях
        /// </summary>
        _Empty = 0,
        Renissans = 1,
        Alpha = 2,
        Guideh = 3,
        Soglasie = 4,
        Absolute = 5,
        Tinkoff = 6
    }

    /// <summary>
    /// id доп. опций страхования взр
    /// </summary>
    public enum enumTravelRisk
    { 
        /// <summary>
        /// Медицинские расходы
        /// </summary>
        medicalExpenses = 1,
        /// <summary>
        /// Активный отдыха
        /// </summary>
        activeRest = 2,
        /// <summary>
        /// Гражданская ответственность 
        /// </summary>
        liability = 3,
        /// <summary>
        /// Отмена рейса
        /// </summary>
        tripCanceled = 4,
        /// <summary>
        /// НС
        /// </summary>
        accident = 5,
        /// <summary>
        /// Любительский спорт
        /// </summary>
        amateurSport = 6,
        /// <summary>
        /// Профессиональный спорт
        /// </summary>
        proSport = 7,
        /// <summary>
        /// Потеря багажа
        /// </summary>
        luggageLoss = 8,
        /// <summary>
        /// юридическая поддержка ??ХЗ
        /// </summary>
        legalSupport = 9,
        /// <summary>
        /// Задержка рейса
        /// </summary>
        flightDelay = 10,
        /// <summary>
        /// Алкогольное опьянение
        /// </summary>
        alcoholic = 11,
        /// <summary>
        /// Беременность
        /// </summary>
        pregnancy = 12,
        /// <summary>
        /// Хронические заболевания
        /// </summary>
        chronicDiseases = 13,
        /// <summary>
        /// Экстремальный спорт
        /// </summary>
        extremeSport = 14
    }

    /// <summary>
    /// тип франшизы
    /// </summary>
    public enum enumFranchiseType
    { 
        /// <summary>
        /// страна
        /// </summary>
        country = 1,
        /// <summary>
        /// доп. опция
        /// </summary>
        additionalOption = 2
    }

    /// <summary>
    /// объекты страхования недвижимости
    /// </summary>
    public enum enumRealtyInsuranceFeature
    { 
        /// <summary>
        /// отделка
        /// </summary>
        decorValue = 27,
        /// <summary>
        /// имущество
        /// </summary>
        propertyValue = 28,
        /// <summary>
        /// гражданская ответственность
        /// </summary>
        liabilityValue = 29,
        /// <summary>
        /// конструктивные элементы
        /// </summary>
        constructiveValue = 30,
        /// <summary>
        /// квартира в аренде
        /// </summary>
        flatInRentValue = 32
    }

    /// <summary>
    /// доп. опции недвижимости
    /// </summary>
    public enum enumRealtyFlatFeature
    {
        /// <summary>
        /// квартира в аренде
        /// </summary>
        isRent = 32,
        /// <summary>
        /// дерево в перекрытиях
        /// </summary>
        woodenBeams = 33,
        /// <summary>
        /// пожарная сигнализация
        /// </summary>
        fireAlarm = 34,
        /// <summary>
        /// охранная сигнализация
        /// </summary>
        alarm = 35,
        /// <summary>
        /// система обнаружения протечки
        /// </summary>
        leakDetection = 36,
        /// <summary>
        /// газ в квартире
        /// </summary>
        gasStove = 37,
        /// <summary>
        /// квартира на последнем этаже
        /// </summary>
        lastFloor = 71
    }

    public enum enumDictionaryTypeId
    {
        /// <summary>
        ///Тип страховки
        /// </summary>
        insuranceType = 2,
        /// <summary>
        ///Доп. опции ВЗР
        /// </summary>
        travelAddOptions = 3,
        /// <summary>
        ///Идентификатор ТС
        /// </summary>
        vehicleId = 4,
        /// <summary>
        ///Документ регистрации ТС
        /// </summary>
        vehicleRegistrationDocument = 5,
        /// <summary>
        ///Страховые риски Недвижимость
        /// </summary>
        realtyRisks = 6,
        /// <summary>
        ///Срок страхования Недвижимости
        /// </summary>
        realtyInsurancePeriod = 7,
        /// <summary>
        ///Объекты страхования Недвижимости
        /// </summary>
        realtyInsuranceObject = 8,
        /// <summary>
        ///Характеристики квартиры
        /// </summary>
        realtyFlatFeatures = 9,
        /// <summary>
        ///Срок страхования НС
        /// </summary>
        accidentInsurancePeriod = 10,
        /// <summary>
        ///Страховые риски НС
        /// </summary>
        accidentRisks = 11,
        /// <summary>
        ///Территория страхования Зеленая карта
        /// </summary>
        greenCardInsuranceArea = 12,
        /// <summary>
        ///Тип транспортного средства Зеленая карта
        /// </summary>
        greenCardVehicleType = 13
    }

    /// <summary>
    /// Период страхование, dictinary typeid = 7
    /// </summary>
    public enum enumInsurancePeriodId
    {
        /// <summary>
        /// Для не-недвижки
        /// </summary>
        notRealty = 0,
        /// <summary>
        /// на год
        /// </summary>
        oneYear = 24,
        /// <summary>
        /// на отпуск
        /// </summary>
        forVacation = 25
    }

    /// <summary>
    /// Гайде, квартира
    /// </summary>
    public enum enumGuidehUrl
    {
        /// <summary>
        /// Логин Гайде Квартира
        /// </summary>
        login,
        /// <summary>
        /// Расчет котировки Гайде Квартира
        /// </summary>
        calculate,
        /// <summary>
        /// Сохранение Гайде Квартира
        /// </summary>
        save,
        /// <summary>
        /// Подтверждение оплаты Гайде Квартира
        /// </summary>
        paymentData,
        /// <summary>
        /// Печать Гайде Квартира
        /// </summary>
        getPolicy
    }

    /// <summary>
    /// статусы полиса (_dictionary typeId = 1)
    /// </summary>
    public enum enumPolicyStatus
    {
        /// <summary>
        /// сохранён
        /// </summary>
        saved = 1,
        /// <summary>
        /// акцептован
        /// </summary>
        accepted = 2,
        /// <summary>
        /// распечатан
        /// </summary>
        printed = 3,
        /// <summary>
        /// аннулирован
        /// </summary>
        annuled = 4
    }
    
    /// <summary>
    ///Коды ответов
    /// </summary>
    public enum enumErrorType
    {
        /// <summary>
        /// 200 Запрос выполнен успешно
        /// </summary>
        ok = 200,
        /// <summary>
        /// 400	Некорректный запрос Не передан параметр rKey.Невалидный JSON.Неверная структура для данного метода
        /// </summary>
        badRequest = 400,
        /// <summary>
        /// 401	Не авторизован  Некорректные реквизиты авторизации.Не передан авторизационный токен. Авторизационный токен недействителен
        /// </summary>
        unauthorized = 401,
        /// <summary>
        /// 404	Не найдено  Сущность с переданным ID не найдена
        /// </summary>
        notFound = 404,
        /// <summary>
        /// 409	Конфликт бизнес-логики Изменения не могут быть применены к экземпляру сущности с указанным ID из за нарушения правил бизнес-логики
        /// </summary>
        logicConflict = 409,
        /// <summary>
        /// 500	Ошибка сервера  Временные перебои в работе сервиса.
        /// </summary>
        serverError = 500,
        /// <summary>
        /// 501	Запрошенный метод не существует Вызван несуществующий метод
        /// </summary>
        methodNotFound = 501,
        /// <summary>
        /// 503	Обновление данных   Временное прекращение работы сервиса в связи с обновлением базы данных
        /// </summary>
        temporarilyUnavailable = 503,
        /// <summary>
        /// 523	Ошибка внешнего хоста Возвращается когда для обработки агентского запроса система выполняет запрос к API вендора, а вендор вернул ошибку*/
        /// </summary>
        externalError = 523,
        /// <summary>
        /// Внутренняя ошибка
        /// </summary>
        internalError = 999
    }

    /// <summary>
    /// Валюта страхования
    /// </summary>
    public enum enumCurrency
    {
        USD = 1,
        EUR = 2
    }

    /// <summary>
    /// Статус оплаты полиса (_dictionary.typeId = 1)
    /// </summary>
    public enum enumPolicyPaymentStatus
    {
        /// <summary>
        /// ожидает оплаты
        /// </summary>
        awaiting = 1,
        /// <summary>
        /// оплачен
        /// </summary>
        paid = 2,
        /// <summary>
        /// ошибка оплаты полиса, либо оплата была прервана пользователем
        /// </summary>
        error = 3
    }
}