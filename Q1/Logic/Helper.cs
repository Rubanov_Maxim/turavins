﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using SMPL.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using DataModels;
using DataModels.DB;
using TuravIns.Models;

namespace TuravIns.Logic
{
    /// <summary>
    /// Класс для хранения различных методов вне общей классификации
    /// </summary>
    public class Helper
    {
        IDB DB = TuravIns.Creator.DB;

        /// <summary>
        /// Делает из объекта xml-ину
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string getRawXml(object obj)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            StringBuilder xmlSb = new StringBuilder();
            StringWriter textWriter = new StringWriter(xmlSb);
            xmlSerializer.Serialize(textWriter, obj);
            string returnSerializedXML = xmlSb.ToString();
            textWriter.Close();

            return returnSerializedXML;
        }

        /// <summary>
        /// Переводит текст в unicode escape
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string getUnicodeString(string s)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char c in s)
            {
                builder.Append("\\u");
                builder.Append(String.Format("{0:x4}", (int)c));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Проверка адреса электронной почты
        /// </summary>
        /// <param name="email"></param>
        /// <returns>true - адрес НЕ подходит</returns>
        public bool checkEmailAddressIsBad(string email)
        {
            string regEx = "^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-z_]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-z_])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9]))$";
            Regex regex1 = new Regex(regEx);
            MatchCollection res1 = regex1.Matches(email.ToLower());

            return res1.Count != 1;
        }

        ///// <summary>
        ///// Получить путь до полиса вида .\policyFiles\{имя + ид агента}\{тип страхования}\{ск}\{guid полиса}.pdf
        ///// </summary>
        ///// <param name="agentModel">агент (нужны имя и id)</param>
        ///// <param name="policyGuid">гуид полиса</param>
        ///// <param name="company">компания</param>
        ///// <param name="type">тип страхования</param>
        ///// <returns></returns>
        //public string policyPathGet(DataModels.agent agentModel, string policyGuid, enumInsuranceCompany company, enumInsuranceType type)
        //{
        //    string saveFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "policyFiles", $"{agentModel.name}_{agentModel.id}", Dictionaries.dictionaryInsuranceTypeFolder[type], Dictionaries.dictionaryInsuranceCompanyFolder[company]);

        //    SMPL.Helpers.Creator.fileHelperInternal.folderCheck(saveFolder);

        //    return Path.Combine(saveFolder, $"{policyGuid}.pdf");
        //}

        /// <summary>
        /// Слепить страницы нескольких документов pdf в один документ
        /// </summary>
        /// <param name="finalDoc"></param>
        /// <param name="listPdfPage"></param>
        public byte[] pdfPagesConcatenate(List<byte[]> listPdfPage)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Document document = new Document();

                PdfWriter writer = PdfWriter.GetInstance(document, stream);

                //document.SetPageSize(PageSize.LETTER)
                document.Open();
                PdfContentByte contentByte = writer.DirectContent;
                PdfImportedPage page;

                PdfReader reader;
                foreach (byte[] pdf in listPdfPage)
                {
                    reader = new PdfReader(pdf);
                    int pages = reader.NumberOfPages;

                    for (int i = 1; i <= pages; i++)
                    {
                        document.NewPage();
                        page = writer.GetImportedPage(reader, i);
                        contentByte.AddTemplate(page, 0, 0);
                    }
                }

                document.Close();
                byte[] finalDoc = stream.GetBuffer();
                stream.Flush();

                return finalDoc;
            }
        }

        /// <summary>
        /// Метод добавляет любое кол-во get параметров в url ,если такие параметры уже есть в исходном url,то заменяет их значения
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="collectionParams">NameValueCollection с параметрами</param>
        /// <returns></returns>
        public string addParamToUrl(string url, System.Collections.Specialized.NameValueCollection collectionParams)
        {
            string urlBody = url;
            string urlTail = "";
            if (url.Contains("?"))
            {
                urlBody = url.Substring(0, url.IndexOf("?"));
                urlTail = url.Substring(url.IndexOf("?"));

            }
            System.Collections.Specialized.NameValueCollection urlParameter = HttpUtility.ParseQueryString(urlTail);
            collectionParams.AllKeys.ToList().ForEach(paramName =>
            {
                if (urlParameter[paramName] != null) { urlParameter[paramName] = collectionParams[paramName]; }
                else { urlParameter.Add(paramName, collectionParams[paramName]); }
            });

            string returnUrl = urlBody + "?" + urlParameter.ToString();

            return returnUrl;
        }

        ///// <summary>
        ///// Получение курса валют с сайта центробанка(только евро и доллары)
        ///// https://dwprdev.atlassian.net/wiki/spaces/APIENGINE/pages/445448821
        ///// </summary>
        ///// <param name="currencyId"></param>
        ///// <returns></returns>
        //public decimal currencyExchangeRateGet(int currencyId)
        //{
        //    //получаем список всех валют из дб
        //    List<currency> listCurrencyDB = DB.listCurrencyGet();
        //    //Актуальный курс хранится в таблице currencyExchangeRate. 
        //    //Если при обращении к таблице курса на сегодня еще нет -нужно сходить на сайт cbr.ru получить курсы для евро и доллара и записать в currencyExchangeRate.
        //    //Потом взять значение для нужной валюты и применить в нужном месте
        //    currencyExchangeRate currencyExchangeRate = DB.listCurrencyExchangeRateByCurrencyIdGet(currencyId).
        //        Where(x => x.date == DateTime.Now.Date)?.LastOrDefault() ?? new currencyExchangeRate();

        //    if (currencyExchangeRate.id != 0) { return currencyExchangeRate.value; }
        //    else
        //    {
        //        int counter = 0;
        //        while (true)
        //        {
        //            try
        //            {
        //                string returnXML = string.Empty;
        //                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.cbr.ru/scripts/XML_daily.asp");
        //                request.Method = "GET";
        //                request.CookieContainer = new CookieContainer();
        //                HttpWebResponse HTTPresponseCur = (HttpWebResponse)request.GetResponse();
        //                using (Stream dataStream = HTTPresponseCur.GetResponseStream())
        //                using (StreamReader reader = new StreamReader(dataStream))
        //                {
        //                    returnXML = reader.ReadToEnd();
        //                }
        //                XmlDocument xml = new XmlDocument();
        //                xml.LoadXml(returnXML);
        //                XmlNodeList xnList = xml.SelectNodes("/ValCurs/Valute");

        //                decimal eur = 0;
        //                decimal usd = 0;
        //                //знаение запрашиваемой валюты
        //                decimal returnValueCurrency = 0;
        //                currencyExchangeRate.date = DateTime.Now;

        //                //смотрим полученный список и добавляем в бд курсы евро и доллара
        //                foreach (XmlNode xn in xnList)
        //                {
        //                    if (xn["CharCode"].InnerText == "USD")
        //                    {
        //                        usd = Decimal.Parse(xn["Value"].InnerText);
        //                        currencyExchangeRate.value = usd;
        //                        currencyExchangeRate.currencyId = listCurrencyDB.Where(x => x.name == "USD").FirstOrDefault().id;
        //                        DB.currencyExchangeRateInsert(currencyExchangeRate);
        //                    }
        //                    if ((xn["CharCode"].InnerText == "EUR"))
        //                    {
        //                        eur = Decimal.Parse(xn["Value"].InnerText);
        //                        currencyExchangeRate.value = eur;
        //                        currencyExchangeRate.currencyId = listCurrencyDB.Where(x => x.name == "EUR").FirstOrDefault().id;
        //                        DB.currencyExchangeRateInsert(currencyExchangeRate);
        //                    }
        //                }
        //                //лезем опять в бд и забираем нужное значение. На случай если понадобятся еещ валюты
        //                returnValueCurrency = DB.listCurrencyExchangeRateByCurrencyIdGet(currencyId).Where(x => x.date == DateTime.Now.Date)?.LastOrDefault()?.value ?? 0;
        //                if (returnValueCurrency == 0) { throw new Exception(ErrorMessage.HELPERMODELEXCEPTION_CURRENCYEXCHEANGEERROR); }
        //                return returnValueCurrency;
        //            }
        //            catch (Exception ex)
        //            {
        //                //пробуем получить валюту 5 раз. Если не выйдет, проваливаемся в ошибку
        //                if (counter++ < 5)
        //                {
        //                    System.Threading.Thread.Sleep(1000);    // выжидаем секунду перед следующей попыткой
        //                    continue;
        //                }
        //                throw new Exception($"{ex.Message} {ErrorMessage.HELPERMODELEXCEPTION_CURRENCYEXCHEANGEERROR}");
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// проверяет наличие кириллицы в строке
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool stringContainsCyrillic(string text)
        {
            return text.ToLower().Any(c => "абвгдеёжзийклмнопрстуфхцчшщъыьэюя".Contains(c));
        }

        /// <summary>
        /// проверяет наличие латиницы в строке
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool stringContainsLatin(string text)
        {
            return text.ToLower().Any(c => "abcdefghijklmnopqrstuvwxyz".Contains(c));
        }

        /// <summary>
        /// определить IP адрес текущего компьютера
        /// </summary>
        /// <returns></returns>
        public string getIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Не найден сетевой адаптер с адресом IPv4");
        }

        /// <summary>
        /// оставляет только цифры телефона
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public string clearPhone(string phone)
        {
            return phone.Replace(" ", "").Replace("-", "").Replace("+", "").Replace("(", "").Replace(")", "");
        }

        /// <summary>
        /// возвращает возраст
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public int getAge(DateTime birthdate)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - birthdate.Year;
            //откатываем 1 год, если взяли много
            if (birthdate > today.AddYears(-age)) age--;

            return age;
        }
    }
}