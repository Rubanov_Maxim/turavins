﻿using TuravIns.Models.AddressFinder;

namespace TuravIns.Logic
{
    public interface IAddressFinder
    {
        AddressSuggestionResponseModel suggestionsGet(AddressSuggestionRequestModel addressRequest);
    }
}