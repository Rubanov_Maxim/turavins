﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace TuravIns.Logic
{
    /// <summary>
    /// Хелпер для запросов в сторонние службы, получающими и отдающими контент в json.
    /// Как использовать:
    /// 1) Создаем объект JsonPayloadHttpWebRequest 
    /// 2) В его свойстве Request настраиваем хэдеры и пр. свойства, если нужно
    /// 3) Вызываем метод DoRequest(), возвращающий ответ в десериализованном виде
    /// </summary>
    /// <typeparam name="TRequest">Тип модели, в котором мы ожидаем ответ</typeparam>
    /// <typeparam name="TResponse">Тип модели, которую мы отправляем в запросе</typeparam>
    public class JsonPayloadHttpWebRequest<TRequest, TResponse>
    {
        private HttpWebRequest request;
        public HttpWebRequest Request
        {
            get { return request; }
        }

        private TRequest clientRequestPayload;

        private HttpStatusCode lastResponseStatusCode = HttpStatusCode.OK;
        public HttpStatusCode LastResponseStatusCode => lastResponseStatusCode;

        public JsonPayloadHttpWebRequest(string uri, TRequest request)
        {
            this.request = (HttpWebRequest)WebRequest.Create(uri);
            this.request.Method = "POST";
            this.request.KeepAlive = true;

            clientRequestPayload = request;
        }


        public TResponse doRequest()
        {
            //мы работаем только с json
            //TODO: сделать проверку и генерить иксепшн если клиент поставил не json
            request.ContentType = @"application/json;charset=UTF-8";
            request.Accept = @"application/json";

            string jsonQuery = SMPL.Helpers.Creator.json.serializeModel(clientRequestPayload);

            byte[] bytes = (new UTF8Encoding()).GetBytes(jsonQuery);

            request.ContentLength = bytes.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
            }

            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                lastResponseStatusCode = response.StatusCode;
            }
            catch (WebException e)
            {
                HttpWebResponse exceptionResponse = (HttpWebResponse)e.Response;
                lastResponseStatusCode = exceptionResponse.StatusCode;
                throw;
            }
            string jsonResponse = string.Empty;

            using (Stream dataStream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    jsonResponse = reader.ReadToEnd();
                }
            }

            return SMPL.Helpers.Creator.json.deserializeModel<TResponse>(jsonResponse);
        }
    }
}