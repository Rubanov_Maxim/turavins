﻿using TuravIns.Models.AddressFinder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace TuravIns.Logic
{
    public class DaData : IDaData
    {
        private static readonly string BASE_URI = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/";

        private readonly string TOKEN = "163b771459fb8b0c07464ef3773c506cb8de6732"; //бесплатный тестовый токен
        //private readonly string TOKEN = "b061e6e6bf28494aa8d7ec6c57c85e2bf29325f8";  //strahovki24.ru  (купленный токен, возвращает 403, привязан к домену?, нужно отвязать?)  


        public DaData(string token)
        {
            TOKEN = token;
        }

        public DaData()
        {

        }

        /// <summary>
        /// Запрашиваем подсказки у Дадаты
        /// </summary>
        /// <param name="query"></param>
        /// <returns>Ответ с подсказками от Дадаты</returns>
        public DaDataSuggestionResponse addressSuggestionsGet(DaDataSuggestionRequest query)
        {
            string uri = BASE_URI + "address";
            var webRequestWorker = new JsonPayloadHttpWebRequest<DaDataSuggestionRequest, DaDataSuggestionResponse>(uri, query);
            webRequestWorker.Request.Headers.Add("Authorization: Token " + TOKEN);

            DaDataSuggestionResponse response;
            try
            {
                response = webRequestWorker.doRequest();
                daDataRequestLog(((int)webRequestWorker.LastResponseStatusCode).ToString());
            }
            catch (Exception e)
            {
                daDataRequestLog(((int)webRequestWorker.LastResponseStatusCode).ToString());
                throw;
            }

            return response;
        }

        private void daDataRequestLog(string responseCode)
        {
            SMPL.Helpers.Creator.msSqlHelper.execute("INSERT INTO dbo.dadataUsageLog (responseCode) VALUES (@responseCode)", "responseCode", responseCode);
        }

    }
}