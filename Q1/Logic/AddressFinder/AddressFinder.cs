﻿using TuravIns.Models.AddressFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns.Logic
{
    public class AddressFinder : IAddressFinder
    {
        /// <summary>
        /// Сейчас работаем только с Дадата, потом можно добавить другие сервисы
        /// </summary>
        public IDaData daData { get; set; } = new DaData();

        /// <summary>
        /// Получаем запрос на подсказки, возвращаем ответ
        /// </summary>
        /// <param name="suggestionRequest"></param>
        /// <returns></returns>
        public AddressSuggestionResponseModel suggestionsGet(AddressSuggestionRequestModel suggestionRequest)
        {
            DaDataSuggestionRequest daDataRequest = requestForDaDataConvert(suggestionRequest);
            DaDataSuggestionResponse dadataResponse = daData.addressSuggestionsGet(daDataRequest);
            return responseFromDaDataConvert(dadataResponse);
        }


        #region Конвертеры dto в/из дадаты

        private DaDataSuggestionRequest requestForDaDataConvert(AddressSuggestionRequestModel request)
        {
            return new DaDataSuggestionRequest()
            {
                query = request.query,
                count = request.count
            };
        }

        private AddressSuggestionResponseModel responseFromDaDataConvert(DaDataSuggestionResponse response)
            => new AddressSuggestionResponseModel()
            {
                suggestions = new List<AddressSuggestionModel>(
                    response.suggestions.Select(x => new AddressSuggestionModel() { value = x.value, unrestricted_value = x.unrestricted_value, data = x.data }
                ))
            };


    }

    #endregion


}