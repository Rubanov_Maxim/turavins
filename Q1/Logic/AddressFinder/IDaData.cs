﻿using TuravIns.Models.AddressFinder;

namespace TuravIns.Logic
{
    public interface IDaData
    {
        DaDataSuggestionResponse addressSuggestionsGet(DaDataSuggestionRequest query);
    }
}