﻿using SMPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace TuravIns.Logic
{
    public class HttpContextHandler : IHttpContextHandler
    {
        public void returnResponseModel(ResponseModel res, int statusCode)
        {
            HttpContext.Current.Response.ContentType = "application/json;charset=utf-8";
            HttpContext.Current.Response.StatusCode = statusCode;
            HttpContext.Current.Response.Write(SMPL.Helpers.Creator.json.serializeModel(res));
        }

        public void returnFile(string path, string fileName, string mimeType)
        {
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=\"" + WebUtility.UrlEncode(fileName).Replace("+", "%20") + "\"");
            HttpContext.Current.Response.ContentType = mimeType;
            HttpContext.Current.Response.WriteFile(path);
        }

        public void returnFile(byte[] fileBody, string fileName, string mimeType)
        {
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=\"" + WebUtility.UrlEncode(fileName).Replace("+", "%20") + "\"");
            HttpContext.Current.Response.ContentType = mimeType;
            HttpContext.Current.Response.BinaryWrite(fileBody);
        }
    }
}