﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns.Logic
{
    public interface IHttpContextHandler
    {
        /// <summary>
        /// Возвращает ResponseModel
        /// </summary>
        /// <param name="res">The resource.</param>
        /// <param name="statusCode"></param>
        void returnResponseModel(SMPL.Models.ResponseModel res, int statusCode);

        /// <summary>
        /// возвращает файл (через адрес)
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="fileName"></param>
        /// <param name="mimeType"></param>
        void returnFile(string path, string fileName, string mimeType);

        /// <summary>
        /// возвращает файл (через массив байтов)
        /// </summary>
        /// <param name="fileBody"></param>
        /// <param name="fileName"></param>
        /// <param name="mimeType"></param>
        void returnFile(byte[] fileBody, string fileName, string mimeType);
    }
}