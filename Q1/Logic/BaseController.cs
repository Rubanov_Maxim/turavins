﻿using DataModels.DB;

namespace TuravIns.Logic
{
    public class BaseController
    {
        #region common
        internal SMPL.Helpers.IJson smplJson;
        internal SMPL.Helpers.IModelBinder modelBinder;
        internal SMPL.Helpers.IDateHelper date;
        internal IDB DB;
        #endregion

        public BaseController()
        {
            DB = TuravIns.Creator.DB;
            smplJson = SMPL.Helpers.Creator.json;
            modelBinder = SMPL.Helpers.Creator.modelBinder;
            date = SMPL.Helpers.Creator.dateHelper;
        }
    }
}