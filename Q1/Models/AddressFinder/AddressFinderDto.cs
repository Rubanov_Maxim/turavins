﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace TuravIns.Models.AddressFinder
{
    /// <summary>
    /// Модель запроса на подсказки, которую мы получаем от клиента
    /// </summary>
    public class AddressSuggestionRequestModel
    {
        public string query { get; set; } = "";
        public int count { get; set; }
    }

    /// <summary>
    /// Модель подсказок, которую мы отправляем клиенту
    /// </summary>
    public class AddressSuggestionResponseModel
    {
        public List<AddressSuggestionModel> suggestions { get; set; }
    }

    /// <summary>
    /// Элемент подсказки адреса
    /// </summary>
    public class AddressSuggestionModel
    {
        public string value { get; set; } = "";
        public string unrestricted_value { get; set; } = "";
        //TODO: сделать свою модель идентичную DaDataAddressData и конвертер 
        public DaDataAddressData data { get; set; }
    }


}