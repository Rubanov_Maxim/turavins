﻿
namespace TuravIns.Models
{
    //свалка универсальных моделей общего назначения
    public class ValueLabelModel
    {
        public string value { get; set; }
        public string label { get; set; }
    }
}