﻿using DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns.Models.Travel
{
    #region income model (agent => apiRouter)
    /// <summary>
    /// параметры для расчета или сохранения полиса (модель, которую получам в теле запроса от агента)
    /// </summary>
    public class TravelIncomeCalculateModel : TravelIncomeBaseModel
    {
        /// <summary>
        /// Гуид котировки из таблицы travelQuotation
        /// </summary>
        public string quotationGuid { get; set; }
        /// <summary>
        /// id страховой компании
        /// </summary>
        public int? insuranceCompanyId { get; set; }
    }

    /// <summary>
    /// модель расширения (та часть, которую сохраняем в travelQuotation.quotationModelJson)
    /// </summary>
    public class TravelIncomeBaseModel
    {
        /// <summary>
        /// Дата начала действия полиса.
        /// </summary>
        [SMPL.Attributes.isoDate]
        public string startDate { get; set; }
        /// <summary>
        /// Дата окончания действия полиса
        /// </summary>
        [SMPL.Attributes.isoDate]
        public string endDate { get; set; }
        /// <summary>
        /// Признак того что полис приобретается для многократных поездок
        /// </summary>
        public bool? isMultiple { get; set; }
        /// <summary>
        /// Идентификатор валюты (справочник currency)
        /// </summary>
        public int? currencyId { get; set; }
        /// <summary>
        /// Количество дней покрытия рисков по полису
        /// </summary>
        public int? coveredDay { get; set; }
        /// <summary>
        /// Данные покупателя полиса (страхователя)
        /// </summary>
        public InsurerModel insurer { get; set; }
        /// <summary>
        /// Массив стран в которых действует полис
        /// </summary>
        public List<CountryModel> country { get; set; }
        /// <summary>
        /// Массив информации о застрахованных по полису
        /// </summary>
        public List<InsuredModel> insured { get; set; }
        /// <summary>
        /// Массив рисков включенных в полис и их сумм покрытия
        /// </summary>
        public List<RiskModel> risk { get; set; }
        /// <summary>
        /// массив особых (специальных) условий.
        /// </summary>
        //public List<SpecialConditionModel> specialCondition { get; set; }
        /// <summary>
        /// причина аннулирования
        /// </summary>
        //public string annulReason { get; set; }
    }

    /// <summary>
    /// данные покупателя полиса (страхователя)
    /// </summary>
    public class InsurerModel : PersonModel
    {
        /// <summary>
        /// Номер телефона в международном формате. Только цифры
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string email { get; set; }
    }

    /// <summary>
    /// страна, в которой действует полис.
    /// </summary>
    public class CountryModel
    {
        /// <summary>
        /// Идентификатор страны (справочник country)
        /// </summary>
        public int? id { get; set; }
        /// <summary>
        /// Название страны (из таблицы country)
        /// </summary>
        public string name { get; set; }
    }

    /// <summary>
    /// данные туриста
    /// </summary>
    public class InsuredModel : PersonModel
    {
        /// <summary>
        /// Возраст туриста (необходим для первичного расчёта котировки)
        /// </summary>
        public int age { get; set; }
    }

    /// <summary>
    /// Модель с общими полями туриста/покупателя
    /// </summary>
    public class PersonModel
    { 
        /// <summary>
        /// Дата рождения
        /// </summary>
        [SMPL.Attributes.isoDate]
        public string birthDate { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string lastName { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string firstName { get; set; }
        /// <summary>
        /// Серия документа удостоверяющего личность
        /// </summary>
        public string documentSeries { get; set; }
        /// <summary>
        /// Номер документа удостоверяющего личность
        /// </summary>
        public string documentNo { get; set; }
        /// <summary>
        /// id полиса, относящегося к страхуемому
        /// </summary>
        //public int policyId { get; set; }
    }

    /// <summary>
    /// идентификаторы рисков и размеры сумм покрытия по ним
    /// </summary>
    public class RiskModel
    {
        /// <summary>
        /// Идентификатор риска (справочник travelRisk)
        /// </summary>
        public int? id { get; set; }
        /// <summary>
        /// Сумма страхового покрытия по риску
        /// </summary>
        public int? coverageSum { get; set; }
        /// <summary>
        /// Название риска (из таблицы travelRisk)
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// франшизы
        /// </summary>
        public List<FranchiseModel> franchise { get; set; }
    }

    /// <summary>
    /// фрашиза для риска
    /// </summary>
    public class FranchiseModel
    {
        /// <summary>
        /// текст или значение
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// id страны
        /// </summary>
        public int? countryId { get; set; }
    }

    /// <summary>
    /// особые условия
    /// </summary>
    public class SpecialConditionModel
    {
        /// <summary>
        /// тип
        /// </summary>
        public int typeId { get; set; }
        /// <summary>
        /// id страны
        /// </summary>
        public int? countryId { get; set; }
        /// <summary>
        /// id риска
        /// </summary>
        public int? riskId { get; set; }
        /// <summary>
        /// флаг многократных поездок
        /// </summary>
        public bool? isMultiple { get; set; }
        /// <summary>
        /// текст
        /// </summary>
        public string value { get; set; }
    }

    /// <summary>
    /// общая входная модель для аксептации, печати и аннулирования
    /// </summary>
    public class TravelIncomeAcceptPrintAnnulModel
    {
        /// <summary>
        /// Гуид котировки полисы которой нужно акцептовать
        /// </summary>
        public string quotationGuid { get; set; }
        /// <summary>
        /// Параметры платежной транзакции
        /// </summary>
        public TransactionModel transaction { get; set; }
        /// <summary>
        /// Описание причины отмены
        /// </summary>
        public string reason { get; set; }
    }

    /// <summary>
    /// Данные об оплате полиса
    /// </summary>
    public class TransactionModel
    {
        /// <summary>
        /// Идентификатор транзакции в платежной системе
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Дата и время списания средств у покупателя полиса
        /// </summary>
        [SMPL.Attributes.isoDateTime]
        public string date { get; set; }
        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public decimal amount { get; set; }
    }

    /// <summary>
    /// модель для проверки существования информации для риска в зависимости от isMultiple
    /// </summary>
    public class AgentRiskBasicModel
    {
        public List<RiskBaseModel> risk { get; set; }

        public class RiskBaseModel
        {
            public bool isMultiple { get; set; }
        }
    }
    #endregion

    #region apiRouter <= insuranceType.Travel
    public class TravelQuotationOutModel : InsuranceOutBaseModel
    {
        /// <summary>
        /// Гуид котировки
        /// </summary>
        public int quotationId { get; set; }
        /// <summary>
        /// Гуид котировки
        /// </summary>
        public string quotationGuid { get; set; }
        /// <summary>
        /// Стоимость полиса
        /// </summary>
        public decimal price { get; set; }
        /// <summary>
        /// сервисная компания
        /// </summary>
        public AssistanceModel assistance { get; set; }
        /// <summary>
        /// ссылки на документы
        /// </summary>
        public DocLinkModel docLink { get; set; }
        /// <summary>
        /// Массив рисков включенных в полис и их сумм покрытия
        /// </summary>
        public List<RiskModel> risk { get; set; }
        /// <summary>
        /// Массив специальных условий (разные модели, поэтому массив объектов)
        /// </summary>
        public List<SpecialConditionModel> specialCondition { get; set; }
    }

    public class TravelPrintOutModel : InsuranceOutBaseModel
    {
        /// <summary>
        /// Массив урлов для скачивания файлов
        /// </summary>
        public List<string> fileUrl { get; set; }
    }

    public class TravelPolicyInfoOutModel : TravelIncomeBaseModel
    {
        /// <summary>
        /// стоимость полиса
        /// </summary>
        public decimal price { get; set; }
        /// <summary>
        /// список ссылок на скачивание полисов
        /// </summary>
        public List<string> listPolicyFileUrl { get; set; }
        /// <summary>
        /// информация по страховой компании
        /// </summary>
        public InsuranceCompanyModel insuranceCompany { get; set; }

        /// <summary>
        /// инфа по рискам
        /// </summary>
        public List<TravelRiskModel> listTravelRisk { get; set; }
        /// <summary>
        /// Массив специальных условий (разные модели, поэтому массив объектов)
        /// </summary>
        public List<SpecialConditionModel> specialCondition { get; set; }

        public class InsuranceCompanyModel
        {
            /// <summary>
            /// id страховой компании
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// название страховой компании
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// телефон СК
            /// </summary>
            public string phone { get; set; }
            /// <summary>
            /// email СК
            /// </summary>
            public string email { get; set; }
            /// <summary>
            /// ассистанс (название)
            /// </summary>
            public string assistanceName { get; set; }
            /// <summary>
            /// ассистанс (телефон)
            /// </summary>
            public string assistancePhone { get; set; }
            /// <summary>
            /// ссылка на правила страхования
            /// </summary>
            public string insuranceRulesLink { get; set; }
        }
    }
    #endregion

    #region модель со справочниками
    public class ListModel
    {
        /// <summary>
        /// список стран
        /// </summary>
        public List<CountryDictionaryModel> listCountry { get; set; }
        /// <summary>
        /// страховые компании
        /// </summary>
        public List<InsuranceCompanyModel> listInsuranceCompany { get; set; }
        /// <summary>
        /// валюты
        /// </summary>
        public List<currency> listCurrency { get; set; }
        /// <summary>
        /// инфа по рискам
        /// </summary>
        public List<TravelRiskModel> listTravelRisk { get; set; }
    }

    public class InsuranceCompanyModel
    {
        /// <summary>
        /// id СК
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// название СК
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// флаг - ФИО допустимы только на кириллице
        /// </summary>
        public bool isCyrillicOnly { get; set; }
    }

    /// <summary>
    /// список стран
    /// </summary>
    public class CountryDictionaryModel : CountryModel
    {
        /// <summary>
        /// флаг - избранная страна
        /// </summary>
        public bool isHighlighted { get; set; }
        /// <summary>
        /// флаг - является страной шенгенского союза
        /// </summary>
        public bool isShengen { get; set; }
    }

    /// <summary>
    /// страховой риск
    /// </summary>
    public class TravelRiskModel : DataModels.travelRisk
    {
        /// <summary>
        /// список сумм покрытия для риска
        /// </summary>
        public List<int> listCoverageSum { get; set; }
    }
    #endregion

    #region выходные модели для FE
    public class QuotationCalculateResult
    {
        /// <summary>
        /// Стоимость полиса
        /// </summary>
        public decimal price { get; set; }
        /// <summary>
        /// Массив специальных условий (разные модели, поэтому массив объектов)
        /// </summary>
        public List<SpecialConditionModel> specialCondition { get; set; }
        /// <summary>
        /// сервисная компания
        /// </summary>
        public AssistanceModel assistance { get; set; }
        /// <summary>
        /// ссылки на документы
        /// </summary>
        public DocLinkModel docLink { get; set; }

        /// <summary>
        /// модель для расчёта
        /// </summary>
        public TravelIncomeCalculateModel calculationModel { get; set; }
        /// <summary>
        /// инфа по рискам
        /// </summary>
        public List<TravelRiskModel> listTravelRisk { get; set; }
    }
    #endregion

    /// <summary>
    /// Информация сервисной компании
    /// </summary>
    public class AssistanceModel
    {
        /// <summary>
        /// имя
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string phone { get; set; }
    }

    /// <summary>
    /// ссылки на документы
    /// </summary>
    public class DocLinkModel
    {
        /// <summary>
        /// ссылка на образец полиса
        /// </summary>
        public string samplePolicyLink { get; set; }
        /// <summary>
        /// ссылка на правила страхования
        /// </summary>
        public string insuranceRulesLink { get; set; }
    }
}