﻿using TuravIns.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns.Models
{
    /// <summary>
    /// модель для ошибок в апи
    /// </summary>
    public class ErrorModel : InsuranceOutBaseModel
    {
        /// <summary>
        /// описание ошибки. Объект с полями
        /// </summary>
        public ErrorSubModel error { get; set; }
        /// <summary>
        /// описание ошибки. Объект с полями
        /// </summary>
        public class ErrorSubModel
        {
            /// <summary>
            /// текст сообщения об ошибке
            /// </summary>
            /// <value>
            /// The message.
            /// </value>
            public string message { get; set; }
            /// <summary>
            /// точное место где произошла ошибка
            /// </summary>
            public string stackTrace { get; set; }
            /// <summary>
            /// код ошибки. В текучие версии API значение errorCode дублирует значение из HTTP-кода заголовка ответа. В перспективе errorCode будет уточнять и детализировать ошибки
            /// </summary>
            /// <value>
            /// The error code.
            /// </value>
            public int errorCode { get; set; }
        }
    }

    /// <summary>
    /// базовый ответ
    /// </summary>
    public class ApiDefaultResponseModel { public string rKey { get; set; } }

    #region модели для логина
    /// <summary>
    /// Выходная модель для логина
    /// </summary>
    public class LoginOutModel : InsuranceOutBaseModel
    {
        /// <summary>
        /// ключ доступа. Guid
        /// </summary>
        public string token { get; set; }
    }
    /// <summary>
    /// Входная модель для логина
    /// </summary>
    public class LoginInModel
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string login { get; set; } = "";
        /// <summary>
        /// Пароль
        /// </summary>
        public string password { get; set; } = "";
    }
    #endregion

    /// <summary>
    /// Базовый класс ответа после запроса в страховую компанию.
    /// </summary>
    public class InsuranceOutBaseModel
    {
        /// <summary>
        /// переданный в URL запроса аргумент rKey
        /// </summary>
        public string rKey { get; set; }
    }

    /// <summary>
    /// Модель с полями, которые необходимо вернуть после логгирования для отправки письма об ошибке
    /// </summary>
    public class logBasicModel
    {
        /// <summary>
        /// Идентификатор вендора (страховой компании, эквайринга, оператора СМС и пр) {vendorId}
        /// </summary>
        public int vendorId { get; set; }
        /// <summary>
        /// Идентификатор сущности в бизнес логике (полис) {logicPolicyId}
        /// </summary>
        public int quotationId { get; set; }
        /// <summary>
        /// Текст запроса для выборки записи из лога внешнего API модуля вида select * from dbo._apiLog l where id = {apiLogId}");
        /// </summary>
        public string logSqlRequest { get; set; }
    }
}
