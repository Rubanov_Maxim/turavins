﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TuravIns
{
    public class BspbinsCustomHandler : IHttpHandler
    {
        public BspbinsCustomHandler() { }

        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            //сюда должен проваливаться, когда запрашивает .html GET-запросами
            //проверка на то, содержит ли адрес iframe-integration - если да, адрес отличается

            //проверка на то,что iis перегрузил сборку и мы потеряли статические списки сессий
            SMPL.Helpers.Creator.sessionManager.listActiveSessionGet();
            string method = context.Request.HttpMethod;
            string path = context.Request.Path;

            if (!method.Equals("GET")) { throw new Exception("How did I get here again?"); }
            //если нашли исключение по пути,то переключаем на контроллер и меняем метод на пост если соответсвие полное или начинается с .../api*
            SMPL.Models.LoadOptionsModel.RouteExceptionModel routeExceptionModel = SMPL.Core.loadOptions.listRouteException.FirstOrDefault(x => x.url == path || path.StartsWith(x.url.Replace("*", "")));
            if (routeExceptionModel != null)
            {
                path = routeExceptionModel.methodPath;
                method = "POST";
            }

            //добавляем заголовки для работы CORS
            string Origin = context.Request.Headers["Origin"] ?? "*";
            if (Origin.Contains("http")) { Origin = Origin.Substring(Origin.IndexOf("http")); }
            context.Response.Headers.Add("Access-Control-Allow-Origin", Origin);
            context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

            //HttpContext.Current.Request.FilePath
            //если мы пытаемся получить файл (с расширением) - лезем обратно в обработчик в SMPL (потому что Logic.FileManager - internal)
            if (path.Contains("."))
            {
                proccessFile(context);
            }
            //в противном случае обрабатываем на месте
            else
            {
                #region вырубил кэш для iframe -Антон жаловался, что из-за него нифига не работает
                //
                //if (context.Request.Headers["If-Modified-Since"] != null &&
                //        SMPL.Helpers.Creator.fileHelperExternal.fileLastChangeDateTimeGet(path).ToString("yyyy-MM-dd HH:mm:ss").Equals(
                //            DateTime.Parse(context.Request.Headers["If-Modified-Since"]).ToString("yyyy-MM-dd HH:mm:ss")))
                //{
                //    context.Response.Cache.SetCacheability(HttpCacheability.Private);
                //    context.Response.Cache.AppendCacheExtension("no-cache, must-revalidate");
                //    context.Response.StatusCode = 304; //nothing changed
                //}
                //else
                //{
                //    string ret1 = loadFileByUrl(path);

                //    if (context.Request.Headers["If-Modified-Since"] != null)
                //    {
                //        context.Response.AddHeader("If-Modified-Since-Checked", DateTime.Parse(context.Request.Headers["If-Modified-Since"]).AddHours(-3).ToString("ddd, dd MMM yyyy HH:mm:ss", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " GMT");
                //    }
                //    else
                //    {
                //        context.Response.AddHeader("If-Modified-Since-Checked", "If-Modified-Since is empty");
                //    }
                //    context.Response.Cache.SetCacheability(HttpCacheability.Private);
                //    context.Response.Cache.AppendCacheExtension("no-cache, must-revalidate");
                //    context.Response.Cache.SetLastModified(SMPL.Helpers.Creator.fileHelperExternal.fileLastChangeDateTimeGet(path));
                //    context.Response.Cache.SetExpires(DateTime.Now.AddMonths(1));
                //    context.Response.Write(ret1);
                //}
                #endregion


                string ret1 = loadFileByUrl(path);
                context.Response.Cache.SetCacheability(HttpCacheability.Private);
                context.Response.Cache.AppendCacheExtension("no-cache, must-revalidate");
                context.Response.Cache.SetLastModified(SMPL.Helpers.Creator.fileHelperExternal.fileLastChangeDateTimeGet(path));
                context.Response.Cache.SetExpires(DateTime.Now.AddMonths(1));
                context.Response.Write(ret1);
            }
        }

        /// <summary>
        /// Обработка запрошенного адреса, загрузка нужной страницы
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string loadFileByUrl(string path)
        {
            string fileIoPath = path.ToLower().Contains("iframe-integration") ? "/Views/iframe-integration/index.html" : "/Views/index.html";
            fileIoPath = fileIoPath.Replace("'", "`");
            fileIoPath = System.Web.Hosting.HostingEnvironment.MapPath(fileIoPath);
            return System.IO.File.ReadAllText(fileIoPath);
        }

        //MimeMapping.GetMimeMapping(file.fileName)
        public void proccessFile(HttpContext context)
        {
            string filename = "/Views" + context.Request.Path;
            string fileExtension = context.Request.Path.Substring(context.Request.Path.LastIndexOf(".") + 1);
            string mimeType = MimeMapping.GetMimeMapping("file." + fileExtension);
            if (!string.IsNullOrEmpty(mimeType))
            {
                //context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                context.Response.ContentType = mimeType;

                if (new string[] { "js", "svg", "css", "html" }.Contains(fileExtension)) //добавляем gzip сжатие
                {
                    context.Response.Filter = new System.IO.Compression.GZipStream(context.Response.Filter, System.IO.Compression.CompressionMode.Compress);
                    context.Response.Headers.Remove("Content-Encoding");
                    context.Response.AddHeader("Content-Encoding", "gzip");
                }
            }

            try
            {
                //if (context.Request.Headers["If-Modified-Since"] != null &&
                //    SMPL.Helpers.Creator.fileHelperExternal.fileLastChangeDateTimeGet(context.Request.FilePath).ToString("yyyy-MM-dd HH:mm:ss").Equals(
                //        DateTime.Parse(context.Request.Headers["If-Modified-Since"]).ToString("yyyy-MM-dd HH:mm:ss")))
                //{
                //    context.Response.Cache.SetCacheability(HttpCacheability.Private);
                //    context.Response.Cache.AppendCacheExtension("no-cache, must-revalidate");
                //    context.Response.StatusCode = 304; //nothing changed
                //}
                //else
                //{
                    if (context.Request.Headers["If-Modified-Since"] != null)
                    {
                        context.Response.AddHeader("If-Modified-Since-Checked", DateTime.Parse(context.Request.Headers["If-Modified-Since"]).AddHours(-3).ToString("ddd, dd MMM yyyy HH:mm:ss", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) + " GMT");
                    }
                    else
                    {
                        context.Response.AddHeader("If-Modified-Since-Checked", "If-Modified-Since is empty");
                    }
                    context.Response.Cache.SetCacheability(HttpCacheability.Private);
                    context.Response.Cache.AppendCacheExtension("no-cache, must-revalidate");
                    context.Response.Cache.SetLastModified(SMPL.Helpers.Creator.fileHelperExternal.fileLastChangeDateTimeGet(context.Request.FilePath));
                    context.Response.Cache.SetExpires(DateTime.Now.AddMonths(1));
                    context.Response.TransmitFile(filename);
                //}
            }
            catch (Exception)
            {
                context.Response.StatusCode = 404;
                context.Response.End();
                return;
            }
        }
    }
}