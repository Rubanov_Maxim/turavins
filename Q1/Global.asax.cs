﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Reflection;

namespace TuravIns
{
    public class Global : System.Web.HttpApplication
    {
        public static string connectionString { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["main"].ConnectionString;

            System.Globalization.CultureInfo info1 = new System.Globalization.CultureInfo("ru-RU");
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = info1;
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = info1;

            SMPL.Models.LoadOptionsModel loadOptions = new SMPL.Models.LoadOptionsModel()
            {
                connectionString = connectionString,
                controllerNamespace = "TuravIns.Controllers",
                isUnitTest = System.Configuration.ConfigurationManager.AppSettings["configuration"].Equals("UnitTest"),
                parentAssembly = Assembly.GetExecutingAssembly(),
                security = new SMPL.Models.LoadOptionsModel.SecurityModel()
                {
                    isCookieCheckEnabled = false,
                    //cookieName = "bspbins_visit",
                    isCheckRoles = false,
                    listRouteWithoutCookie = new List<string>() { }//"/api/" }
                },
                //Позволяет выйти post запросом на метод или один метод на другой (что трейсим/на что меняем)
                listRouteException = new List<SMPL.Models.LoadOptionsModel.RouteExceptionModel>()
                {
                    new SMPL.Models.LoadOptionsModel.RouteExceptionModel() { methodPath = "/PolicyCheck/policycheck", url = "/PolicyCheck/policycheck" },
                    //new SMPL.Models.LoadOptionsModel.RouteExceptionModel() { methodPath = "/api/ApiRouter/routeResponse", url = "/api/*" }
                }
            };
            loadOptions.email = new SMPL.Helpers.EmailHelper.EmailProviderConfigModel()
            {
                senderName = "turavins",
                enableSsl = true,
                smtpLogin = "notification@dwpr.ru",
                smtpPass = "kA9Q5wLypT",
                smtpPort = 587,
                smtpServerAddress = "mail.cashmyvisit.ru",
                smtpIsLoginEmail = true
            };

            //if (System.Configuration.ConfigurationManager.AppSettings["configuration"].Equals("Dev")) //для локальных тестов екваиринга,чтобы на гет смотреть
            //{
                loadOptions.listRouteException.Add(new SMPL.Models.LoadOptionsModel.RouteExceptionModel() { methodPath = "/payment/catchresponse", url = "/payment/catchresponse" });
            //}
            
            SMPL.Core.load(loadOptions);
        }


        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string newUrl = Context.Request.Url.ToString();
            string[] badUrlArray = 
                {
                    //"bspbins.ru",     //https://dwprdev.atlassian.net/browse/BSPBINS-1143
                    "strahovki24.com"
                };

            if (newUrl.Contains("/PolicyCheck/policycheck") || newUrl.Contains("/ReportSender/borderoSend"))
            {
                return;
            }
            foreach (string badUrl in badUrlArray)
            {
                if (newUrl.Contains(badUrl))
                {
                    newUrl = newUrl.Replace(badUrl, "strahovki24.ru");
                }
            }
            //рерут блога
            if (newUrl.Contains("https://strahovki24.ru/blog"))
            {
                newUrl = newUrl.Replace("https://strahovki24.ru/blog", "http://blog.strahovki24.ru/");
            }

            //if (newUrl.Contains("https://strahovki24.dwpr.ru/blog"))
            //{
            //    newUrl = newUrl.Replace("https://strahovki24.dwpr.ru/blog", "http://blog.strahovki24.ru/");
            //}

            //для того чтобы работало у разработчиков be(localhost),fe(bspbins.dwpr.ru) заменяем на https не все адреса
            else if (!Context.Request.IsSecureConnection)
            {
                if (Context.Request.Url.Host != "localhost" && !Context.Request.Url.Host.Contains("turavins"))
                {
                    newUrl = newUrl.Replace("http:", "https:");
                }
            }

            if (newUrl != Context.Request.Url.ToString())
            {
                Response.Redirect(newUrl);
            }

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}