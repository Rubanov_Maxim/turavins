﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using DataModels.DB;
using TuravIns.Models;

namespace TuravIns.API
{
    public interface IPostRequest
    {
        /// <summary>
        /// пост-запрос
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        string postRequest(string url, string json);
        
        /// <summary>
        /// получение токена
        /// </summary>
        /// <returns></returns>
        string tokenGet();

        /// <summary>
        /// составление ответа в случае возникновения ошибки в insapi
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        SMPL.Models.ResponseModel responseErrorModelGet(WebException ex);
    }

    public class PostRequest : IPostRequest
    {
        internal IDB DB;
        internal SMPL.Helpers.IJson smplJson;

        public PostRequest()
        {
            DB = TuravIns.Creator.DB;
            smplJson = SMPL.Helpers.Creator.json;
        }

        /// <summary>
        /// post-запрос
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public string postRequest(string url, string json)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.KeepAlive = true;
            request.UserAgent = @"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";
            request.ContentType = @"application/json;charset=UTF-8";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Host = "apiengine-dev.dwpr.ru";
            request.Accept = "application/json";
            request.CookieContainer = new CookieContainer();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            using (Stream requestWriter = request.GetRequestStream())
            {
                requestWriter.Write(Encoding.UTF8.GetBytes(json), 0, Encoding.UTF8.GetByteCount(json));
            }

            string returnJson = "";
            //пока не сможем понять, почему он продолжает ругаться на SSL/TLS, сделаем так
            //грубо доверяем
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                returnJson = reader.ReadToEnd();
            }

            //перестаем грубо доверять
            System.Net.ServicePointManager.ServerCertificateValidationCallback = null;

            return returnJson;
        }
        
        /// <summary>
        /// получение токена
        /// </summary>
        /// <returns></returns>
        public string tokenGet()
        {
            LoginInModel inModel = new LoginInModel()
            {
                login = System.Configuration.ConfigurationManager.AppSettings["turavLogin"].ToString(),
                password = System.Configuration.ConfigurationManager.AppSettings["turavPassword"].ToString()
            };
            string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString + $"/api/security/v1/agentLogin?rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";

            string responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(inModel));
            LoginOutModel outModel = smplJson.deserializeModel<LoginOutModel>(responseJson);
            
            return outModel.token;
        }

        /// <summary>
        /// составление ответа в случае возникновения ошибки в insapi
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public SMPL.Models.ResponseModel responseErrorModelGet(WebException ex)
        {
            HttpStatusCode? status = (ex.Response as HttpWebResponse)?.StatusCode;
            string errorModeljson = "";
            using (Stream dataStream = (ex.Response as HttpWebResponse)?.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                errorModeljson = reader.ReadToEnd();
            }

            ErrorModel errorModel = smplJson.deserializeModel<ErrorModel>(errorModeljson);

            return new SMPL.Models.ResponseModel()
            {
                isOk = false,
                resultText = ((int)status).ToString(),
                data = errorModel.error.message + @"
" + errorModel.error.stackTrace
            };
        }
    }
}