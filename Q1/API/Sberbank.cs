﻿using DataModels.DB;
using SMPL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using TuravIns.Logic;

namespace TuravIns.API
{
    public interface ISberbank
    {
        /// <summary>
        /// содание заказа
        /// </summary>
        /// <param name="quotationGuid">id заказа в ЛК</param>
        /// <param name="orderNumber">содержимое аргумента orderNumber (текст)</param>
        /// <param name="description">содержимое аргумента description (текст)</param>
        /// <param name="price">сумма в рублях</param>
        /// <returns></returns>
        ResponseModel create(string quotationGuid, string orderNumber, string description, decimal price);

        /// <summary>
        /// Проверка оплаты полиса
        /// </summary>
        /// <param name="paymentGuid"></param>
        /// <returns></returns>
        bool checkPayment(string paymentGuid);
    }

    public class Sberbank  : ISberbank
    {
        IDB DB = TuravIns.Creator.DB;
        SMPL.Helpers.IJson smplJson = SMPL.Helpers.Creator.json;

        /// <summary>
        /// содание заказа
        /// </summary>
        /// <param name="quotationGuid">id заказа в ЛК</param>
        /// <param name="price">сумма в рублях</param>
        /// <returns></returns>
        public ResponseModel create(string quotationGuid, string orderNumber, string description, decimal price)
        {
            DataModels.payment payment = new DataModels.payment()
            {
                price = Convert.ToInt32(price * 100),
                creationDate = DateTime.Now,
                guid = SMPL.Helpers.Creator.encryption.guidCreate(1),
                quotationGuid = quotationGuid,
                statusId = (int)enumPolicyPaymentStatus.awaiting
            };

            payment.id = DB.paymentInsert(payment);
            
            //подготовка запроса
            UriBuilder urlBuilder = new UriBuilder("https://3dsec.sberbank.ru/payment/rest/register.do");
            System.Collections.Specialized.NameValueCollection query = HttpUtility.ParseQueryString("");

            query["userName"] = ConfigurationManager.AppSettings["serviceTuravLogin"];
            query["password"] = ConfigurationManager.AppSettings["serviceTuravPassword"];
            query["orderNumber"] = orderNumber;
            //убрал, потому что он переводит всё что здесь будет написано в unicode escape, нам нужен UrlEncode
            //query["description"] = WebUtility.UrlEncode(description); 
            query["amount"] = Convert.ToInt32(price * 100).ToString();
            query["returnUrl"] = ConfigurationManager.ConnectionStrings["paymentCatchResponse"].ConnectionString + $"?result=true&guid={payment.guid}";
            query["failUrl"] = ConfigurationManager.ConnectionStrings["paymentCatchResponse"].ConnectionString + $"?result=false&guid={payment.guid}";

            urlBuilder.Query = query.ToString() + $"&description={WebUtility.UrlEncode(description)}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
            request.Method = "POST";
            request.KeepAlive = true;
            request.UserAgent = @"Apache-HttpClient/4.1.1 (java 1.5)";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Host = "3dsec.sberbank.ru";
            request.ContentType = @"test/plain;charset=utf-8";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            
            //делаем запись в лог
            DataModels.paymentLog paymentLog = new DataModels.paymentLog()
            {
                requestXml = urlBuilder.ToString(),
                creationDate = DateTime.Now,
                methodName = "register",
                paymentId = payment.id
            };

            try
            {
                string resultJson = "";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    resultJson = reader.ReadToEnd();
                }

                paymentLog.responseXml = resultJson;
                SberbankRequestResultModel resultModel = smplJson.deserializeModel<SberbankRequestResultModel>(resultJson);

                if (resultModel.errorCode != 0)
                {
                    throw new Exception(resultModel.errorMessage);
                }

                payment.bankOrderId = resultModel.orderId;

                return new ResponseModel()
                {
                    data = resultModel.formUrl
                };
            }
            catch (Exception ex)
            {
                paymentLog.isError = true;
                payment.statusId = (int)enumPolicyPaymentStatus.error;
                return new ResponseModel()
                {
                    isOk = false,
                    resultText = "Ошибка регистрации заказа в Сбербанке: " + ex.Message
                };
            }
            finally
            {
                DB.paymentUpdate(payment);
                DB.paymentLogInsert(paymentLog);
            }
        }

        /// <summary>
        /// Проверка оплаты полиса
        /// </summary>
        /// <param name="paymentGuid"></param>
        /// <returns></returns>
        public bool checkPayment(string paymentGuid)
        {
            //подготовка запроса
            UriBuilder urlBuilder = new UriBuilder("https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do");
            System.Collections.Specialized.NameValueCollection query = HttpUtility.ParseQueryString("");

            query["userName"] = ConfigurationManager.AppSettings["serviceTuravLogin"];
            query["password"] = ConfigurationManager.AppSettings["serviceTuravPassword"];
            query["orderId"] = paymentGuid;

            urlBuilder.Query = query.ToString();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
            request.Method = "POST";
            request.KeepAlive = true;
            request.UserAgent = @"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Host = "3dsec.sberbank.ru";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            try
            {
                string resultJson = "";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    resultJson = reader.ReadToEnd();
                }

                SberbankPaymentStatusResultModel resultModel = smplJson.deserializeModel<SberbankPaymentStatusResultModel>(resultJson);

                if (resultModel.ErrorCode != 0 ||                       //произошла ошибка
                   resultModel.depositAmount < resultModel.Amount ||    //не полная оплата
                   resultModel.OrderStatus != 2)                        //статус оплаты отличен от успешного
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// модель ответа сбербанка на создание заявки на оплату
        /// </summary>
        public class SberbankRequestResultModel
        {
            /// <summary>
            /// id заявки
            /// </summary>
            public string orderId { get; set; }
            /// <summary>
            /// ссылка на оплату
            /// </summary>
            public string formUrl { get; set; }

            /// <summary>
            /// код ошибки
            /// </summary>
            public int errorCode { get; set; }
            /// <summary>
            /// сообщение об ошибке
            /// </summary>
            public string errorMessage { get; set; }
        }

        /// <summary>
        /// Ответ запроса состояния заказа
        /// </summary>
        public class SberbankPaymentStatusResultModel
        {
            /// <summary>
            /// код ошибки
            /// </summary>
            public int ErrorCode { get; set; }
            /// <summary>
            /// сообщение об ошибке
            /// </summary>
            public string ErrorMessage { get; set; }

            /// <summary>
            /// внесённая сумма платежа (?)
            /// </summary>
            public int depositAmount { get; set; }
            /// <summary>
            /// Код валюты платежа ISO 4217
            /// </summary>
            public int currency { get; set; }
            /// <summary>
            /// устаревшее поле, всегда == 2
            /// </summary>
            public int authCode { get; set; }
            /// <summary>
            /// состояние заказа в платежной системе. Если != 2 - ошибка
            /// </summary>
            public int OrderStatus { get; set; }
            /// <summary>
            /// Номер (идентификатор) заказа в системе магазина
            /// </summary>
            public string OrderNumber { get; set; }
            /// <summary>
            /// Сумма платежа в минимальных единицах валюты - в копейках
            /// </summary>
            public int Amount { get; set; }
        }
    }
}