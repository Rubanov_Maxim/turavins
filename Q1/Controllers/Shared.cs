﻿using TuravIns.Logic;
using SMPL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using LinqToDB;
using System.Linq;
using DataModels;

namespace TuravIns.Controllers
{
    public class Shared : BaseController
    {
        public ResponseModel listActiveCompanyGet()
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return new ResponseModel()
                {
                    data = db.insuranceCompanies.Where(x => x.isActive).Select(x => x.name).ToList()
                };
            }
        }
        
        /// <summary>
        /// Метод достаёт юрл сайта из адреса страницы
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        internal string getSiteUrl(string url)
        {
            string siteUrl = url.Replace("http://", "").Replace("https://", "");
            if (siteUrl.StartsWith("www.")) { siteUrl = siteUrl.Substring(4); }

            if (siteUrl.Contains("/")) { siteUrl = siteUrl.Substring(0, siteUrl.IndexOf("/")); }
            
            //Преобразование русских доменов
            if (siteUrl.Contains("xn--"))
            {
                try
                {
                    siteUrl = HttpUtility.UrlDecode(siteUrl);
                    System.Globalization.IdnMapping a = new System.Globalization.IdnMapping();
                    string s = a.GetUnicode(siteUrl);
                    siteUrl = s;
                }
                catch (Exception)
                { }
            }
            //если залетит адрес в юникод
            if (siteUrl.Contains("%"))
            {
                try
                {
                    siteUrl = HttpUtility.UrlDecode(siteUrl);
                }
                catch (Exception)
                { }

            }
            if (siteUrl.Contains("?"))
            {
                siteUrl = siteUrl.Substring(0, siteUrl.IndexOf("?"));
            }

            return siteUrl;
        }
    }
}