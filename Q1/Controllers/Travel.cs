﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMPL.Models;
using TuravIns.Models.Travel;
using TuravIns.Logic;
using TuravIns.Models;
using System.Net;
using System.IO;
using System.Text;

namespace TuravIns.Controllers
{
    public class Travel : BaseController
    {
        /// <summary>
        /// получение справочных списков и модель для расчёта котировки
        /// </summary>
        /// <returns></returns>
        public ResponseModel listModelGet()
        {
            ListModel listModel = new ListModel();

            //страны
            listModel.listCountry = DB.listCountryGet().Select(x => new CountryDictionaryModel()
            {
                id = x.id,
                name = x.name,
                isHighlighted = Dictionaries.listHighlightedCountry.Contains(x.id),
                isShengen = (bool)x.isShengen
            }).OrderBy(x => x.name).ToList();

            //валюты
            listModel.listCurrency = DB.listCurrencyGet();

            //страховые компании
            listModel.listInsuranceCompany = DB.listInsuranceCompanyByIsActiveGet(true).Select(x => 
            new InsuranceCompanyModel()
            {
                id = x.id,
                name = x.name,
                isCyrillicOnly = x.isCyrillicOnly
            }).ToList();

            //список рисков
            listModel.listTravelRisk = DB.listTravelRiskGet().Select(x => modelBinder.copyClass<DataModels.travelRisk, TravelRiskModel>(x)).ToList();

            listModel.listTravelRisk.ForEach(risk =>
            {
                risk.listCoverageSum = DB.listTravelRiskCoverageSumByRiskIdGet(risk.id).Select(x => x.coverageSum).Distinct().OrderBy(x => x).ToList();
            });

            //судя по всему, в базе лишние значения для сумм медицинских рисков - оставляем только нужные
            listModel.listTravelRisk.First(x => x.id == 1).listCoverageSum = new List<int>() { 35000, 50000, 100000 };

            //получаем токен заранее, иначе при расчёте котировки он может попытаться получить по одному на каждую страховую, в результате ничего не будет работать
            string token = "";
            try
            {
                token = Creator.postRequest.tokenGet();
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            return new ResponseModel()
            {
                data = listModel
            };
        }

        /// <summary>
        /// получение модели для расчёта
        /// </summary>
        /// <returns></returns>
        public ResponseModel modelGet()
        {
            TravelIncomeCalculateModel model = new TravelIncomeCalculateModel()
            {
                startDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"),
                insurer = new InsurerModel(),
                insured = new List<InsuredModel>()
                {
                    new InsuredModel()
                },
                country = new List<CountryModel>(),
                risk = new List<RiskModel>()
                {
                    new RiskModel()
                }
            };

            return new ResponseModel()
            {
                data = model
            };
        }

        /// <summary>
        /// расчёт котировки
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public ResponseModel quotationCalculate(string json)
        {
            string token = "";
            try
            {
                token = Creator.postRequest.tokenGet();
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString +
                $"/api/travelInsurance/v1/travelCalculation?token={token}&rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";

            List<DataModels.country> listCountry = DB.listCountryGet();
            TravelIncomeCalculateModel inModel = smplJson.deserializeModel<TravelIncomeCalculateModel>(json);

            //если среди стран есть хотя бы одна с евро, принудительно ставим валюту страхования евро
            inModel.currencyId = listCountry.Where(x => inModel.country.Any(y => y.id == x.id)).Any(x => x.defaultCurrencyId == (int)enumCurrency.EUR) ? (int)enumCurrency.EUR : (inModel.currencyId ?? (int)enumCurrency.EUR);

            //расчитываем др туристов по возрасту, если не передана дата
            inModel.insured.ForEach(tourist =>
            {
                if (string.IsNullOrEmpty(tourist.birthDate))
                {
                    tourist.birthDate = DateTime.Now.AddDays(-30).AddYears(-tourist.age).ToString("yyyy-MM-dd");
                }
            });

            string responseJson = "";
            try
            {
                responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(inModel));
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            TravelQuotationOutModel outModel = smplJson.deserializeModel<TravelQuotationOutModel>(responseJson);

            //поскольку у нас дурацкая система с возрастами/днями рождения, вытаскиваем модель по 2 кругу, чтобы не было мусора
            inModel = smplJson.deserializeModel<TravelIncomeCalculateModel>(json);
            inModel.risk = outModel.risk;
            inModel.quotationGuid = outModel.quotationGuid;

            QuotationCalculateResult result = new QuotationCalculateResult()
            {
                price = outModel.price,
                calculationModel = inModel,
                specialCondition = outModel.specialCondition,
                assistance = outModel.assistance,
                docLink = outModel.docLink
            };

            //собираем риски специально для данной страховой
            List<TravelRiskModel> listTravelRisk = DB.listTravelRiskGet().Select(x => new TravelRiskModel()
            {
                id = x.id,
                name = x.name
            }).ToList();

            List<DataModels.travelRiskCoverageSum> listRiskCoverageSum = DB.listTravelRiskCoverageSumByInsuranceCompanyIdGet((int)inModel.insuranceCompanyId);
            for (int i = listTravelRisk.Count - 1; i >= 0; i--)
            {
                if (!listRiskCoverageSum.Any(x => x.riskId == listTravelRisk[i].id))
                {
                    listTravelRisk.RemoveAt(i);
                    continue;
                }
                listTravelRisk[i].listCoverageSum = listRiskCoverageSum.Where(x => x.riskId == listTravelRisk[i].id).Select(x => x.coverageSum).ToList();
            }

            result.listTravelRisk = listTravelRisk;

            return new ResponseModel()
            {
                data = result
            };
        }

        /// <summary>
        /// сохранение полиса
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public ResponseModel policySave(string json)
        {
            string token = "";
            try
            {
                token = Creator.postRequest.tokenGet();
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString +
                $"/api/travelInsurance/v1/travelSave?token={token}&rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";

            List<DataModels.country> listCountry = DB.listCountryGet();
            TravelIncomeCalculateModel inModel = smplJson.deserializeModel<TravelIncomeCalculateModel>(json);

            //если среди стран есть хотя бы одна с евро, принудительно ставим валюту страхования евро
            inModel.currencyId = listCountry.Where(x => inModel.country.Any(y => y.id == x.id)).Any(x => x.defaultCurrencyId == (int)enumCurrency.EUR) ? (int)enumCurrency.EUR : (inModel.currencyId ?? (int)enumCurrency.EUR);

            string responseJson = "";
            try
            {
                responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(inModel));
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            TravelQuotationOutModel outModel = smplJson.deserializeModel<TravelQuotationOutModel>(responseJson);
            
            string orderNumber = $"{outModel.quotationId}-{DB.listPaymentByQuotationGuidGet(outModel.quotationGuid).Count()}";
            string description = $"Оплата полиса СК {DB.insuranceCompanyGet((int)inModel.insuranceCompanyId).name}";

            return Creator.sberbank.create(outModel.quotationGuid, orderNumber, description, outModel.price);
        }
        
        /// <summary>
        /// печать полиса
        /// </summary>
        /// <param name="json"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public ResponseModel policyPrint(string quotationGuid, string email = "")
        {
            ResponseModel policyDownloadResult = this.policyDownload(quotationGuid);

            if (!policyDownloadResult.isOk) { return policyDownloadResult; }

            List<string> listPolicyUrl = (List<string>)policyDownloadResult.data;

            if (string.IsNullOrEmpty(email))
            {
                //получаем мыло покупателя
                ResponseModel policyInfo = policyInformationGet(quotationGuid);
                email = ((TravelPolicyInfoOutModel)policyInfo.data).insurer.email;
            }

            List<byte[]> listPolicyFiles = new List<byte[]>();
            WebClient webClient = new WebClient();
            foreach (string path in listPolicyUrl)
            {
                listPolicyFiles.Add(webClient.DownloadData(path));
            }
            int i = 0;

            //отправляем письмо с файлами на почту
            SMPL.Helpers.Creator.emailHelper.sendEmail(email,
                    listPolicyUrl.Count == 1 ? "Ваш полис страхования" : "Ваши полисы страхования",
                    (listPolicyUrl.Count == 1 ? "Ваш полис страхования" : "Ваши полисы страхования") + ". https://turavins-dev.dwpr.ru/",
                    listPolicyUrl.Select(path => new SMPL.Helpers.EmailHelper.EmailAttachmentModel()
                        {
                            fileName = path.Split('/').Last().Split('=').Last() + ".pdf",
                            MimeType = "application/pdf",
                            file = listPolicyFiles[i++]
                    }).ToList(), null);

            return new ResponseModel();
        }

        /// <summary>
        /// Получить список ссылок для скачивания полиса напрямую
        /// </summary>
        /// <param name="quotationGuid"></param>
        /// <returns></returns>
        public ResponseModel policyDownload(string quotationGuid)
        {
            string token = "";
            try
            {
                token = Creator.postRequest.tokenGet();
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString +
                $"/api/travelInsurance/v1/travelprint?token={token}&rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";

            string responseJson = "";
            try
            {
                responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(new TravelIncomeAcceptPrintAnnulModel() { quotationGuid = quotationGuid }));
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            TravelPrintOutModel outModel = smplJson.deserializeModel<TravelPrintOutModel>(responseJson);

            return new ResponseModel()
            {
                data = outModel.fileUrl
            };
        }

        /// <summary>
        /// Получение вспомогательной информации о полисе
        /// </summary>
        /// <param name="quotationGuid"></param>
        /// <returns></returns>
        public ResponseModel policyInformationGet(string quotationGuid)
        {
            TravelPolicyInfoOutModel result = new TravelPolicyInfoOutModel();
            try
            {
                string token = Creator.postRequest.tokenGet();

                string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString +
                    $"/api/travelInsurance/v1/travelget?token={token}&rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";

                string responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(new TravelIncomeAcceptPrintAnnulModel() { quotationGuid = quotationGuid }));

                result = smplJson.deserializeModel<TravelPolicyInfoOutModel>(responseJson);
            }
            catch (WebException ex)
            {
                return Creator.postRequest.responseErrorModelGet(ex);
            }

            //расчитываем возраст страхуемых
            result.insured.ForEach(tourist =>
            {
                tourist.age = Convert.ToInt32(Math.Floor((DateTime.Now - date.datetimeFromIsoStringGet(tourist.birthDate)).Days / 365.25M));
            });

            //собираем риски специально для данной страховой
            List<TravelRiskModel> listTravelRisk = DB.listTravelRiskGet().Select(x => new TravelRiskModel()
            {
                id = x.id,
                name = x.name
            }).ToList();

            List<DataModels.travelRiskCoverageSum> listRiskCoverageSum = DB.listTravelRiskCoverageSumByInsuranceCompanyIdGet(result.insuranceCompany.id);
            for (int i = listTravelRisk.Count - 1; i >= 0; i--)
            {
                if (!listRiskCoverageSum.Any(x => x.riskId == listTravelRisk[i].id))
                {
                    listTravelRisk.RemoveAt(i);
                    continue;
                }
                listTravelRisk[i].listCoverageSum = listRiskCoverageSum.Where(x => x.riskId == listTravelRisk[i].id).Select(x => x.coverageSum).ToList();
            }

            result.listTravelRisk = listTravelRisk;

            return new ResponseModel()
            {
                data = result
            };
        }
    }
}