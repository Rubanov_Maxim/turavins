﻿using SMPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml;
using System.Configuration;
using TuravIns.Logic;
using DataModels.DB;
using System.Net;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.IO;
using TuravIns.Models.Travel;
using TuravIns.Models;

namespace TuravIns.Controllers
{
    public class payment
    {       
        /// <summary>
        /// хука для ответа из банка
        /// </summary>
        public void catchresponse()
        {
            IDB DB = TuravIns.Creator.DB;
            SMPL.Helpers.IJson smplJson = SMPL.Helpers.Creator.json;

            //записываем ответ
            DataModels.paymentLog log = new DataModels.paymentLog()
            {
                creationDate = DateTime.Now,
                methodName = "catchResponse",
                requestXml = HttpContext.Current.Request.Url.ToString()
            };

            log.id = DB.paymentLogInsert(log);

            bool result = Convert.ToBoolean(HttpContext.Current.Request.Params["result"].ToString());
            string paymentGuid = HttpContext.Current.Request.Params["guid"].ToString();
            string orderGuid = HttpContext.Current.Request.Params["orderId"].ToString();
            string lang = HttpContext.Current.Request.Params["lang"].ToString();

            string redirectUrl = ConfigurationManager.ConnectionStrings["paymentResponseRedirect"].ConnectionString;
            string quotationGuid = "";
            try
            {
                //проверяем валидность оплаты
                DataModels.payment payment = DB.listPaymentByGuidGet(paymentGuid).FirstOrDefault();

                //неизвестная оплата
                if (payment == null || payment.bankOrderId != orderGuid)
                {
                    log.isError = true;
                    throw new Exception("");
                }

                quotationGuid = payment.quotationGuid;

                //проверяем статус оплаты
                if (!Creator.sberbank.checkPayment(orderGuid))
                {
                    log.isError = true;
                    throw new Exception("");
                }

                //аксептация
                string token = Creator.postRequest.tokenGet();

                string url = System.Configuration.ConfigurationManager.ConnectionStrings["insapiAddress"].ConnectionString +
                    $"/api/travelInsurance/v1/travelaccept?token={token}&rKey={SMPL.Helpers.Creator.encryption.guidCreate(1)}";
                TravelIncomeAcceptPrintAnnulModel inModel = new TravelIncomeAcceptPrintAnnulModel()
                {
                    quotationGuid = quotationGuid,
                    transaction = new TransactionModel()
                    {
                        amount = payment.price / 100M,
                        date = DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
                        id = orderGuid
                    }
                };

                string responseJson = Creator.postRequest.postRequest(url, smplJson.serializeModel(inModel));
                InsuranceOutBaseModel outModel = smplJson.deserializeModel<InsuranceOutBaseModel>(responseJson);

                //печать (возможно, стоит пустить параллельно возврату на FE? но тогда могут быть проблемы при попытке скачать полис)
                ResponseModel printResponse = new Travel().policyPrint(quotationGuid);

                if (!printResponse.isOk)
                {
                    throw new Exception("");
                }

                //всё нормально
                redirectUrl += $"?result=success&quotationGuid={quotationGuid}";
            }
            catch (WebException ex)
            {
                ResponseModel response = Creator.postRequest.responseErrorModelGet(ex);
                //послать письмо админу
                new List<string>() { "mr.scapegrace@gmail.com", "alexandr.popov@dwpr.ru", "dmitry.bogdanov@dwpr.ru" }.ForEach(email => SMPL.Helpers.Creator.emailHelper.sendEmail(email,
                    $"[{ConfigurationManager.ConnectionStrings["applicationServerAddress"].ConnectionString}] TuravIns - Ошибка при аксептации полиса",
                    $@"Произошла ошибка в ходе аксептации полиса: {response.resultText}
data: {response.data.ToString()}
  SELECT TOP 100 * FROM dbo.travelPolicy p WHERE p.quotationId = (SELECT id FROM dbo.travelQuotation q WHERE guid = '{quotationGuid}')"));

                redirectUrl += "?result=error" + (string.IsNullOrEmpty(quotationGuid) ? "" : $"&quotationGuid={quotationGuid}");
            }
            //ошибка (любая)
            catch (Exception ex)
            {
                redirectUrl += "?result=cancel" + (string.IsNullOrEmpty(quotationGuid) ? "" : $"&quotationGuid={quotationGuid}");
                SMPL.Helpers.Creator.msSqlHelper.errorLog(ex.Message, "catchresponse", "", null, ex.ToString());
            }

            //редиректим
            HttpContext.Current.Response.Redirect(redirectUrl, false);
        }
    }
}