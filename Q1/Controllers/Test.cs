﻿using TuravIns.Logic;
using SMPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace TuravIns.Controllers
{
    public class Test
    {
        SMPL.Helpers.IJson json = SMPL.Helpers.Creator.json;

        public ResponseModel rulesGet()
        {
            //new Api.Shared().rulesDocLinkGet(enumInsuranceCompany.Guideh, enumInsuranceType.Realty, enumInsurancePeriodId.forVacation);
            return new ResponseModel();
        }
        ///// <summary>
        ///// ИД валюты 1 - доллар, 2 - евро
        ///// </summary>
        ///// <param name="currencyId"></param>
        ///// <returns></returns>
        //public decimal testCurrencyExchenge(int currencyId)
        //{
        //    //для теста
        //    TuravIns.Logic.Helper helper = new Logic.Helper();
        //    return helper.currencyExchangeRateGet(currencyId);

        //}
        public ResponseModel testBankPayment(string guid)
        {
            return Creator.sberbank.create(guid, "", "", 5);
        }

        public ResponseModel testBankStatus(string guid)
        {
            return new ResponseModel()
            {
                isOk = Creator.sberbank.checkPayment(guid)
            };
        }

        public string testPassword(string password)
        {
            return SMPL.Helpers.Creator.encryption.encrypt(password);
        }

        public string testIP()
        {
            return new Helper().getIPAddress();
        }

        public string jsonTest()
        {
            string json = @"{`id`:`1`, `listName`:[{`name`:`way`},{`name`:`wat1`,`lastName`:`sir`}]}".Replace('`','"');
            TestClass test = SMPL.Helpers.Creator.json.deserializeModel<TestClass>(json);

            return SMPL.Helpers.Creator.json.serializeModel(test);
        }

        public class TestClass
        {
            public int id { get; set; }
            public List<Test1> listName { get; set; }

            public class Test1
            {
                public string name { get;  set; }
            }

            public class Test2 : Test1
            {
                public string lastName { get; set; }
            }
        }
    }
}