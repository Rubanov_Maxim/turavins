using LinqToDB;
using System.Linq;

using System;
using DataModels;
using System.Collections.Generic;
namespace DataModels.DB
{
    public partial class DB :IDB
    {
        SMPL.Helpers.IModelBinder modelBinder = SMPL.Helpers.Creator.modelBinder;
                    public _dictionary dictionaryGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db._dictionary.FirstOrDefault(x => x.id == id);
            }
        }
        public int dictionaryInsert(_dictionary model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void dictionaryDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._dictionary.Delete(x => x.id == id);
            }
        }
        public void dictionaryUpdate(_dictionary model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._dictionary.Where(x => x.id == model.id)
                                                    .Set(p => p.name, model.name)
                                                .Set(p => p.typeId, model.typeId)
                                                .Set(p => p.value, model.value)
                                                .Update();
            }
        } 
                                        
        public List<_dictionary> listDictionaryByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._dictionary.Where(x => x.name == property).ToList();
            }
        }
                                        
        public List<_dictionary> listDictionaryByTypeIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._dictionary.Where(x => x.typeId == property).ToList();
            }
        }
                                        
        public List<_dictionary> listDictionaryByValueGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._dictionary.Where(x => x.value == property).ToList();
            }
        }
                                                                
        public List<_dictionaryType> listDictionaryTypeByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._dictionaryTypes.Where(x => x.name == property).ToList();
            }
        }
                                        public _mailLog mailLogGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db._mailLogs.FirstOrDefault(x => x.id == id);
            }
        }
        public int mailLogInsert(_mailLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void mailLogDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._mailLogs.Delete(x => x.id == id);
            }
        }
        public void mailLogUpdate(_mailLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._mailLogs.Where(x => x.id == model.id)
                                                    .Set(p => p.creationDate, model.creationDate)
                                                .Set(p => p.email, model.email)
                                                .Set(p => p.subject, model.subject)
                                                .Set(p => p.body, model.body)
                                                .Set(p => p.mailServerMessageId, model.mailServerMessageId)
                                                .Set(p => p.sentStatus, model.sentStatus)
                                                .Set(p => p.errorText, model.errorText)
                                                .Update();
            }
        } 
                                        
        public List<_mailLog> listMailLogByCreationDateGet(DateTime property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.creationDate == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogByEmailGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.email == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogBySubjectGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.subject == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogByBodyGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.body == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogByMailServerMessageIdGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.mailServerMessageId == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogBySentStatusGet(Byte property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.sentStatus == property).ToList();
            }
        }
                                        
        public List<_mailLog> listMailLogByErrorTextGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._mailLogs.Where(x => x.errorText == property).ToList();
            }
        }
                                        public _sqlErrorLog sqlErrorLogGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db._sqlErrorLogs.FirstOrDefault(x => x.id == id);
            }
        }
        public int sqlErrorLogInsert(_sqlErrorLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void sqlErrorLogDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._sqlErrorLogs.Delete(x => x.id == id);
            }
        }
        public void sqlErrorLogUpdate(_sqlErrorLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db._sqlErrorLogs.Where(x => x.id == model.id)
                                                    .Set(p => p.logDateTime, model.logDateTime)
                                                .Set(p => p.functionName, model.functionName)
                                                .Set(p => p.lineNumber, model.lineNumber)
                                                .Set(p => p.callStack, model.callStack)
                                                .Set(p => p.messageText, model.messageText)
                                                .Set(p => p.query, model.query)
                                                .Set(p => p.queryParsed, model.queryParsed)
                                                .Update();
            }
        } 
                                        
        public List<_sqlErrorLog> listSqlErrorLogByLogDateTimeGet(DateTime property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.logDateTime == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByFunctionNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.functionName == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByLineNumberGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.lineNumber == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByCallStackGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.callStack == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByMessageTextGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.messageText == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByQueryGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.query == property).ToList();
            }
        }
                                        
        public List<_sqlErrorLog> listSqlErrorLogByQueryParsedGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db._sqlErrorLogs.Where(x => x.queryParsed == property).ToList();
            }
        }
                                        public country countryGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.countries.FirstOrDefault(x => x.id == id);
            }
        }
        public int countryInsert(country model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void countryDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.countries.Delete(x => x.id == id);
            }
        }
        public void countryUpdate(country model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.countries.Where(x => x.id == model.id)
                                                    .Set(p => p.name, model.name)
                                                .Set(p => p.defaultCurrencyId, model.defaultCurrencyId)
                                                .Set(p => p.isShengen, model.isShengen)
                                                .Update();
            }
        } 
                                        
        public List<country> listCountryByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.countries.Where(x => x.name == property).ToList();
            }
        }
                                        
        public List<country> listCountryByDefaultCurrencyIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.countries.Where(x => x.defaultCurrencyId == property).ToList();
            }
        }
                                                public currency currencyGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.currencies.FirstOrDefault(x => x.id == id);
            }
        }
        public int currencyInsert(currency model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void currencyDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.currencies.Delete(x => x.id == id);
            }
        }
        public void currencyUpdate(currency model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.currencies.Where(x => x.id == model.id)
                                                    .Set(p => p.name, model.name)
                                                .Update();
            }
        } 
                                        
        public List<currency> listCurrencyByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.currencies.Where(x => x.name == property).ToList();
            }
        }
                                        public insuranceCompany insuranceCompanyGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.insuranceCompanies.FirstOrDefault(x => x.id == id);
            }
        }
        public int insuranceCompanyInsert(insuranceCompany model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void insuranceCompanyDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.insuranceCompanies.Delete(x => x.id == id);
            }
        }
        public void insuranceCompanyUpdate(insuranceCompany model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.insuranceCompanies.Where(x => x.id == model.id)
                                                    .Set(p => p.name, model.name)
                                                .Set(p => p.isActive, model.isActive)
                                                .Set(p => p.isCyrillicOnly, model.isCyrillicOnly)
                                                .Update();
            }
        } 
                                        
        public List<insuranceCompany> listInsuranceCompanyByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.insuranceCompanies.Where(x => x.name == property).ToList();
            }
        }
                                        
        public List<insuranceCompany> listInsuranceCompanyByIsActiveGet(Boolean property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.insuranceCompanies.Where(x => x.isActive == property).ToList();
            }
        }
                                        
        public List<insuranceCompany> listInsuranceCompanyByIsCyrillicOnlyGet(Boolean property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.insuranceCompanies.Where(x => x.isCyrillicOnly == property).ToList();
            }
        }
                                        public payment paymentGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.payments.FirstOrDefault(x => x.id == id);
            }
        }
        public int paymentInsert(payment model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void paymentDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.payments.Delete(x => x.id == id);
            }
        }
        public void paymentUpdate(payment model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.payments.Where(x => x.id == model.id)
                                                    .Set(p => p.guid, model.guid)
                                                .Set(p => p.quotationGuid, model.quotationGuid)
                                                .Set(p => p.bankOrderId, model.bankOrderId)
                                                .Set(p => p.price, model.price)
                                                .Set(p => p.statusId, model.statusId)
                                                .Set(p => p.creationDate, model.creationDate)
                                                .Update();
            }
        } 
                                        
        public List<payment> listPaymentByGuidGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.payments.Where(x => x.guid == property).ToList();
            }
        }
                                        
        public List<payment> listPaymentByQuotationGuidGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.payments.Where(x => x.quotationGuid == property).ToList();
            }
        }
                                        
        public List<payment> listPaymentByBankOrderIdGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.payments.Where(x => x.bankOrderId == property).ToList();
            }
        }
                                        
        public List<payment> listPaymentByPriceGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.payments.Where(x => x.price == property).ToList();
            }
        }
                                        
        public List<payment> listPaymentByStatusIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.payments.Where(x => x.statusId == property).ToList();
            }
        }
                                                public paymentLog paymentLogGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.paymentLogs.FirstOrDefault(x => x.id == id);
            }
        }
        public int paymentLogInsert(paymentLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void paymentLogDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.paymentLogs.Delete(x => x.id == id);
            }
        }
        public void paymentLogUpdate(paymentLog model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.paymentLogs.Where(x => x.id == model.id)
                                                    .Set(p => p.paymentId, model.paymentId)
                                                .Set(p => p.methodName, model.methodName)
                                                .Set(p => p.requestXml, model.requestXml)
                                                .Set(p => p.responseXml, model.responseXml)
                                                .Set(p => p.creationDate, model.creationDate)
                                                .Set(p => p.isError, model.isError)
                                                .Update();
            }
        } 
                                        
        public List<paymentLog> listPaymentLogByPaymentIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.paymentLogs.Where(x => x.paymentId == property).ToList();
            }
        }
                                        
        public List<paymentLog> listPaymentLogByMethodNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.paymentLogs.Where(x => x.methodName == property).ToList();
            }
        }
                                        
        public List<paymentLog> listPaymentLogByRequestXmlGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.paymentLogs.Where(x => x.requestXml == property).ToList();
            }
        }
                                        
        public List<paymentLog> listPaymentLogByResponseXmlGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.paymentLogs.Where(x => x.responseXml == property).ToList();
            }
        }
                                                
        public List<paymentLog> listPaymentLogByIsErrorGet(Boolean property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.paymentLogs.Where(x => x.isError == property).ToList();
            }
        }
                                        public travelRisk travelRiskGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.travelRisks.FirstOrDefault(x => x.id == id);
            }
        }
        public int travelRiskInsert(travelRisk model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void travelRiskDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.travelRisks.Delete(x => x.id == id);
            }
        }
        public void travelRiskUpdate(travelRisk model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.travelRisks.Where(x => x.id == model.id)
                                                    .Set(p => p.name, model.name)
                                                .Set(p => p.description, model.description)
                                                .Set(p => p.typeId, model.typeId)
                                                .Update();
            }
        } 
                                        
        public List<travelRisk> listTravelRiskByNameGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRisks.Where(x => x.name == property).ToList();
            }
        }
                                        
        public List<travelRisk> listTravelRiskByDescriptionGet(String property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRisks.Where(x => x.description == property).ToList();
            }
        }
                                        
        public List<travelRisk> listTravelRiskByTypeIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRisks.Where(x => x.typeId == property).ToList();
            }
        }
                                        public travelRiskCoverageSum travelRiskCoverageSumGet(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.travelRiskCoverageSums.FirstOrDefault(x => x.id == id);
            }
        }
        public int travelRiskCoverageSumInsert(travelRiskCoverageSum model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 return Convert.ToInt32(db.InsertWithIdentity(model));
            }
        }
        public void travelRiskCoverageSumDelete(int id)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.travelRiskCoverageSums.Delete(x => x.id == id);
            }
        }
        public void travelRiskCoverageSumUpdate(travelRiskCoverageSum model)
        {
            modelBinder.modelNullValueReplace(model);
            using (TuravInsDB db = new TuravInsDB())
            {
                 db.travelRiskCoverageSums.Where(x => x.id == model.id)
                                                    .Set(p => p.riskId, model.riskId)
                                                .Set(p => p.insuranceCompanyId, model.insuranceCompanyId)
                                                .Set(p => p.coverageSum, model.coverageSum)
                                                .Update();
            }
        } 
                                        
        public List<travelRiskCoverageSum> listTravelRiskCoverageSumByRiskIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRiskCoverageSums.Where(x => x.riskId == property).ToList();
            }
        }
                                        
        public List<travelRiskCoverageSum> listTravelRiskCoverageSumByInsuranceCompanyIdGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRiskCoverageSums.Where(x => x.insuranceCompanyId == property).ToList();
            }
        }
                                        
        public List<travelRiskCoverageSum> listTravelRiskCoverageSumByCoverageSumGet(Int32 property)
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                 return db.travelRiskCoverageSums.Where(x => x.coverageSum == property).ToList();
            }
        }
                            }
}
    