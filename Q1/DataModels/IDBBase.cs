using System;
using System.Collections.Generic;
namespace DataModels.DB{
    public partial interface IDB
    {
            /// <summary>
        /// получаем модель по id
        /// </summary>
        _dictionary dictionaryGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int dictionaryInsert(_dictionary model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void dictionaryDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void dictionaryUpdate(_dictionary model);
                
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<_dictionary> listDictionaryByNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    TypeId           
        /// </summary>
        List<_dictionary> listDictionaryByTypeIdGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    Value           
        /// </summary>
        List<_dictionary> listDictionaryByValueGet(String property);
                        
                        
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<_dictionaryType> listDictionaryTypeByNameGet(String property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        _mailLog mailLogGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int mailLogInsert(_mailLog model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void mailLogDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void mailLogUpdate(_mailLog model);
                
                
        /// <summary>
        ///получаем лист моделей по    CreationDate           
        /// </summary>
        List<_mailLog> listMailLogByCreationDateGet(DateTime property);
                        
        /// <summary>
        ///получаем лист моделей по    Email           
        /// </summary>
        List<_mailLog> listMailLogByEmailGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    Subject           
        /// </summary>
        List<_mailLog> listMailLogBySubjectGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    Body           
        /// </summary>
        List<_mailLog> listMailLogByBodyGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    MailServerMessageId           
        /// </summary>
        List<_mailLog> listMailLogByMailServerMessageIdGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    SentStatus           
        /// </summary>
        List<_mailLog> listMailLogBySentStatusGet(Byte property);
                        
        /// <summary>
        ///получаем лист моделей по    ErrorText           
        /// </summary>
        List<_mailLog> listMailLogByErrorTextGet(String property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        _sqlErrorLog sqlErrorLogGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int sqlErrorLogInsert(_sqlErrorLog model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void sqlErrorLogDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void sqlErrorLogUpdate(_sqlErrorLog model);
                
                
        /// <summary>
        ///получаем лист моделей по    LogDateTime           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByLogDateTimeGet(DateTime property);
                        
        /// <summary>
        ///получаем лист моделей по    FunctionName           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByFunctionNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    LineNumber           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByLineNumberGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    CallStack           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByCallStackGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    MessageText           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByMessageTextGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    Query           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByQueryGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    QueryParsed           
        /// </summary>
        List<_sqlErrorLog> listSqlErrorLogByQueryParsedGet(String property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        country countryGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int countryInsert(country model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void countryDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void countryUpdate(country model);
                
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<country> listCountryByNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    DefaultCurrencyId           
        /// </summary>
        List<country> listCountryByDefaultCurrencyIdGet(Int32 property);
                                
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        currency currencyGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int currencyInsert(currency model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void currencyDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void currencyUpdate(currency model);
                
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<currency> listCurrencyByNameGet(String property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        insuranceCompany insuranceCompanyGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int insuranceCompanyInsert(insuranceCompany model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void insuranceCompanyDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void insuranceCompanyUpdate(insuranceCompany model);
                
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<insuranceCompany> listInsuranceCompanyByNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    IsActive           
        /// </summary>
        List<insuranceCompany> listInsuranceCompanyByIsActiveGet(Boolean property);
                        
        /// <summary>
        ///получаем лист моделей по    IsCyrillicOnly           
        /// </summary>
        List<insuranceCompany> listInsuranceCompanyByIsCyrillicOnlyGet(Boolean property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        payment paymentGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int paymentInsert(payment model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void paymentDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void paymentUpdate(payment model);
                
                
        /// <summary>
        ///получаем лист моделей по    Guid           
        /// </summary>
        List<payment> listPaymentByGuidGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    QuotationGuid           
        /// </summary>
        List<payment> listPaymentByQuotationGuidGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    BankOrderId           
        /// </summary>
        List<payment> listPaymentByBankOrderIdGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    Price           
        /// </summary>
        List<payment> listPaymentByPriceGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    StatusId           
        /// </summary>
        List<payment> listPaymentByStatusIdGet(Int32 property);
                                
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        paymentLog paymentLogGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int paymentLogInsert(paymentLog model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void paymentLogDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void paymentLogUpdate(paymentLog model);
                
                
        /// <summary>
        ///получаем лист моделей по    PaymentId           
        /// </summary>
        List<paymentLog> listPaymentLogByPaymentIdGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    MethodName           
        /// </summary>
        List<paymentLog> listPaymentLogByMethodNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    RequestXml           
        /// </summary>
        List<paymentLog> listPaymentLogByRequestXmlGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    ResponseXml           
        /// </summary>
        List<paymentLog> listPaymentLogByResponseXmlGet(String property);
                                
        /// <summary>
        ///получаем лист моделей по    IsError           
        /// </summary>
        List<paymentLog> listPaymentLogByIsErrorGet(Boolean property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        travelRisk travelRiskGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int travelRiskInsert(travelRisk model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void travelRiskDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void travelRiskUpdate(travelRisk model);
                
                
        /// <summary>
        ///получаем лист моделей по    Name           
        /// </summary>
        List<travelRisk> listTravelRiskByNameGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    Description           
        /// </summary>
        List<travelRisk> listTravelRiskByDescriptionGet(String property);
                        
        /// <summary>
        ///получаем лист моделей по    TypeId           
        /// </summary>
        List<travelRisk> listTravelRiskByTypeIdGet(Int32 property);
                        
                        /// <summary>
        /// получаем модель по id
        /// </summary>
        travelRiskCoverageSum travelRiskCoverageSumGet(int id);
        /// <summary>
        /// добавляем модель в базу и получаем id
        /// </summary>
        int travelRiskCoverageSumInsert(travelRiskCoverageSum model);
        /// <summary>
        /// удаляем модель по id
        /// </summary>
        void travelRiskCoverageSumDelete(int id);
        /// <summary>
        /// обновляем модель 
        /// </summary>
        void travelRiskCoverageSumUpdate(travelRiskCoverageSum model);
                
                
        /// <summary>
        ///получаем лист моделей по    RiskId           
        /// </summary>
        List<travelRiskCoverageSum> listTravelRiskCoverageSumByRiskIdGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    InsuranceCompanyId           
        /// </summary>
        List<travelRiskCoverageSum> listTravelRiskCoverageSumByInsuranceCompanyIdGet(Int32 property);
                        
        /// <summary>
        ///получаем лист моделей по    CoverageSum           
        /// </summary>
        List<travelRiskCoverageSum> listTravelRiskCoverageSumByCoverageSumGet(Int32 property);
                        
                                                                                                                                    }
}
    