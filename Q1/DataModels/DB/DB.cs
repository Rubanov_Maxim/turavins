﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels.DB
{
    /// <summary>
    /// методы для общения с БД, которые не создаются автоматически в DBBase
    /// </summary>
    public partial class DB : IDB
    {
        /// <summary>
        /// Список всех валют из ДБ
        /// </summary>
        /// <returns></returns>
        public List<currency> listCurrencyGet()
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.currencies.ToList();
            }
        }

        /// <summary>
        /// Список всех стран из БД
        /// </summary>
        /// <returns></returns>
        public List<country> listCountryGet()
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.countries.ToList();
            }
        }
        
        /// <summary>
        /// Список всех рисков из БД
        /// </summary>
        /// <returns></returns>
        public List<travelRisk> listTravelRiskGet()
        {
            using (TuravInsDB db = new TuravInsDB())
            {
                return db.travelRisks.ToList();
            }
        }

        ///// <summary>
        ///// последний созданный токен
        ///// </summary>
        ///// <returns></returns>
        //public token lastTokenGet()
        //{
        //    using (TuravInsDB db = new TuravInsDB())
        //    {
        //        return db.tokens.Where(x => x.cretionDateTime > DateTime.Now.AddMinutes(-55)).OrderByDescending(x => x.cretionDateTime).FirstOrDefault();
        //    }
        //}
    }
}