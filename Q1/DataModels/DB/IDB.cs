﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels.DB
{
    /// <summary>
    /// методы для общения с БД, которые не создаются автоматически в DBBase
    /// </summary>
    public partial interface IDB
    {
        /// <summary>
        /// Список всех валют из БД
        /// </summary>
        /// <returns></returns>
        List<currency> listCurrencyGet();

        /// <summary>
        /// Список всех стран из БД
        /// </summary>
        /// <returns></returns>
        List<country> listCountryGet();

        /// <summary>
        /// Список всех рисков из БД
        /// </summary>
        /// <returns></returns>
        List<travelRisk> listTravelRiskGet();
    }
}