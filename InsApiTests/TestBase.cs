﻿using SMPL.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TuravIns.Logic;
using DataModels.DB;

namespace InsApiTests
{
    [TestClass]
    public class TestBase
    {
        public Mock<DataModels.DB.IDB> dbMock;
        public Mock<TuravIns.Logic.IAddressFinder> addressFinderMock;
        public Mock<TuravIns.Logic.IHttpContextHandler> httpHandlerMock;
        
        [TestInitialize]
        public void initBase()
        {
            SMPL.Testing.TestNinjectModule.LoadTestMocks();

            dbMock = new Mock<DataModels.DB.IDB>();
            addressFinderMock = new Mock<TuravIns.Logic.IAddressFinder>();

            httpHandlerMock = new Mock<IHttpContextHandler>();
            
            SMPL.Testing.Mocks.FileHelperMock.mock = new Mock<SMPL.Helpers.FileHelper.IFileHelper>();
            

            SMPL.Testing.Mocks.FileHelperMock.mock = new Mock<SMPL.Helpers.FileHelper.IFileHelper>();

            TuravIns.Creator.binding = new TuravIns.Creator.BindingModel()
            {
                _DB = dbMock.Object,
                _addressFinder = addressFinderMock.Object,
                _httpContextHandler = httpHandlerMock.Object,
                
            };
        }
    }
}
