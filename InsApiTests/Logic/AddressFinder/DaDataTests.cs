﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TuravIns.Models.AddressFinder;
using TuravIns.Logic;
using TuravInsTests.Logic.AddressFinder.Tests;

namespace TuravInsTests.Logic.DaData.Tests
{
    [TestClass()]
    public class DaDataTests : BaseTests
    {
        [TestMethod()]
        public void addressSuggestionsGet_Requesti7SuggestionsFromDadata_Got7Suggestions()
        {
            //Arrange
            TuravIns.Logic.IDaData dadata = new TuravIns.Logic.DaData();
            var query = new DaDataSuggestionRequest() { query = "Санкт", count = 7 };

            //Act
            DaDataSuggestionResponse response = dadata.addressSuggestionsGet(query);

            //Assert
            Assert.IsTrue(response.suggestions.Count == 7);
        }
    }
}