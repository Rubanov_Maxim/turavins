﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TuravIns;
using TuravIns.Logic;
using TuravIns.Models.AddressFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace TuravInsTests.Logic.AddressFinder.Tests
{
    [TestClass()]
    public class AddressFinderTests : BaseTests
    {
        private Mock<IDaData> daDataMockGenerate(string queryString, int queryCount)
        {
            var mock = new Mock<IDaData>();

            var query = new DaDataSuggestionRequest() { query = queryString, count = queryCount };

            var response = new DaDataSuggestionResponse()
            {
                suggestions = new List<DaDataSuggestionResponse.SuggestionItem>()
            };

            if (!string.IsNullOrEmpty(queryString))
            {

                if (queryCount == 0)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        response.suggestions.Add(new DaDataSuggestionResponse.SuggestionItem() { value = $"ответ{i}", data = new DaDataAddressData() });
                    }
                }

                if (queryCount > 1)
                {
                    for (int i = 0; i < queryCount; i++)
                    {
                        response.suggestions.Add(new DaDataSuggestionResponse.SuggestionItem() { value = $"ответ{i}", data = new DaDataAddressData() });
                    }
                }

            }

            mock.Setup(x => x.addressSuggestionsGet(It.IsAny<DaDataSuggestionRequest>())).Returns(response);

            return mock;

        }

        [TestMethod()]
        public void suggestionsGet_Request2Suggestions_Got2Suggestions()
        {

            Mock<IDaData> daDataMock = daDataMockGenerate("Cанкт", 2);
            TuravIns.Logic.IAddressFinder finder = new TuravIns.Logic.AddressFinder() { daData = daDataMock.Object };
            var request = new AddressSuggestionRequestModel() { query = "Санкт", count = 2 };

            AddressSuggestionResponseModel response = finder.suggestionsGet(request);

            Assert.IsTrue(response.suggestions.Count == 2);
        }

        [TestMethod]
        public void suggestionsGet_Request1Suggestion_Got0Suggestions()
        {
            Mock<IDaData> daDataMock = daDataMockGenerate("Cанкт", 1);
            TuravIns.Logic.IAddressFinder finder = new TuravIns.Logic.AddressFinder() { daData = daDataMock.Object };
            var request = new AddressSuggestionRequestModel() { query = "Санкт", count = 1 };

            AddressSuggestionResponseModel response = finder.suggestionsGet(request);

            Assert.IsTrue(response.suggestions.Count == 0);
        }

        [TestMethod]
        public void suggestionsGet_Request0Suggestion_Got10Suggestions()
        {
            Mock<IDaData> daDataMock = daDataMockGenerate("Cанкт", 0);
            TuravIns.Logic.IAddressFinder finder = new TuravIns.Logic.AddressFinder() { daData = daDataMock.Object };
            var request = new AddressSuggestionRequestModel() { query = "Санкт", count = 0 };

            AddressSuggestionResponseModel response = finder.suggestionsGet(request);

            Assert.IsTrue(response.suggestions.Count == 10);
        }


        [TestMethod]
        public void suggestionsGet_RequestSuggestionsForEmptyAddress_Got0Suggestions()
        {
            Mock<IDaData> daDataMock = daDataMockGenerate("", 2);
            TuravIns.Logic.IAddressFinder finder = new TuravIns.Logic.AddressFinder() { daData = daDataMock.Object };
            var request = new AddressSuggestionRequestModel() { query = "", count = 2 };

            AddressSuggestionResponseModel response = finder.suggestionsGet(request);

            Assert.IsTrue(response.suggestions.Count == 0);
        }


    }
}