﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuravInsTests.Logic.AddressFinder.Tests
{
    public class BaseTests
    {
        [TestInitialize]
        public void Init()
        {
            SMPL.Testing.TestNinjectModule.LoadTestMocks();
        }
    }
}
