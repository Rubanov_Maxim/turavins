﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TuravIns.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuravIns.Logic.Tests
{
    [TestClass()]
    public class HelperTests
    {
        public Helper helper = null;
        [TestInitialize]
        public void init()
        {
            SMPL.Testing.TestNinjectModule.LoadTestMocks();
            helper = new Helper();

        }


        /// <summary>
        /// Проверяем функцию добавления параметров в url под разными углами
        /// </summary>
        [TestCategory("helper")]
        [DataTestMethod()]
        [DataRow("https://nikdwprs.github.io","id", "5", "name", "good", "type", "ok", "https://nikdwprs.github.io?id=5&name=good&type=ok")]
        [DataRow("https://nikdwprs.github.io/", "id", "5", "name", "good", "type", "ok", "https://nikdwprs.github.io/?id=5&name=good&type=ok")]
        [DataRow("https://nikdwprs.github.io?id=3&name=bad", "id", "5", "name", "good", "type", "ok", "https://nikdwprs.github.io?id=5&name=good&type=ok")]
        public void addParamToUrlTest(string baseUrl,string param1,string param1Value,string param2,string param2Value, string param3, string param3Value,string expectedResult)
        {
            System.Collections.Specialized.NameValueCollection urlParams = new System.Collections.Specialized.NameValueCollection();
            urlParams.Add(param1, param1Value);
            urlParams.Add(param2, param2Value);
            urlParams.Add(param3, param3Value);
            Assert.IsTrue(expectedResult == helper.addParamToUrl(baseUrl, urlParams), $"expectedResult={expectedResult} != {helper.addParamToUrl(baseUrl, urlParams)} ");
        }
    }
}